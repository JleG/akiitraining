import { Application, Request, Response, Router } from "express";

import * as equipmentController from "../controllers/equipment.controller";
import { CreateEquipmentDTO, FilterEquipmentDTO, UpdateEquipmentDTO } from "../services/equipment/equipment.dto";

const router = Router();

module.exports = async (app: Application) => {
  router.post("/", async (req: Request, res: Response) => {
    const payload: CreateEquipmentDTO = req.body;
    const results = await equipmentController.create(payload);
    res.status(results.status).json(results.json);
  });

  router.put("/", async (req: Request, res: Response) => {
    const payload: UpdateEquipmentDTO = req.body;
    const results = await equipmentController.update(payload);
    res.status(results.status).json(results.json);
  });

  router.get("/", async (req: Request, res: Response) => {
    const filters: FilterEquipmentDTO = req.body;
    const results = await equipmentController.getAll(filters);
    res.status(results.status).json(results.json);
  });

  router.get("/:id", async (req: Request, res: Response) => {
    const filters: FilterEquipmentDTO = req.body;
    const id: number = Number(req.params.id);
    const results = await equipmentController.getByIds([id], filters);
    res.status(results.status).json(results.json);
  });

  router.delete("/", async (req: Request, res: Response) => {
    const { ids, options } = req.body;

    const results = await equipmentController.deleteByIds(ids, options);
    res.status(results.status).json(results.json);
  });

  app.use("/api/equipment", router);
};
