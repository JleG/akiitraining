import { Application, Request, Response, Router } from "express";
import { async } from "validate.js";

import * as muscleGroupController from "../controllers/muscleGroup.controller";
import { CreateMuscleGroupDTO, FilterMuscleGroupDTO, UpdateMuscleGroupDTO } from "../services/muscleGroup/muscleGroup.dto";

const router = Router();

module.exports = async (app: Application) => {
  router.post("/", async (req: Request, res: Response) => {
    const payload: CreateMuscleGroupDTO = req.body;
    const results = await muscleGroupController.create(payload);
    res.status(results.status).json(results.json);
  });

  router.put("/", async (req: Request, res: Response) => {
    const payload: UpdateMuscleGroupDTO = req.body;
    const results = await muscleGroupController.update(payload);
    res.status(results.status).json(results.json);
  });

  router.get("/", async (req: Request, res: Response) => {
    const filters: FilterMuscleGroupDTO = req.body;
    const results = await muscleGroupController.getAll(filters);
    res.status(results.status).json(results.json);
  });

  router.get("/:id", async (req: Request, res: Response) => {
    const filters: FilterMuscleGroupDTO = req.body;
    const id: number = Number(req.params.id);
    const results = await muscleGroupController.getByIds([id], filters);
    res.status(results.status).json(results.json);
  });

  router.delete("/", async (req: Request, res: Response) => {
    const { ids, options } = req.body;

    const results = await muscleGroupController.deleteByIds(ids, options);
    res.status(results.status).json(results.json);
  });

  app.use("/api/musclegroup", router);
};
