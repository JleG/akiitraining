import { Application, Request, Response } from "express";
import fs from "fs";
import path from "path";

module.exports = (app: Application) => {
  // Simple route
  app.get("/", async (req: Request, res: Response): Promise<Response> => {
    return res.status(200).send({ message: `Welcome to my application stranger.` });
  });

  // List of routes
  fs.readdirSync(__dirname)
    .filter((file: string) => {
      return file.indexOf(".") !== 0 && file.slice(-3) === ".ts" && file !== path.basename(__filename);
    })
    .forEach((file: any) => {
      require(`./${file}`)(app);
    });
};
