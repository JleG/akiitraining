import { Application, Request, Response, Router } from "express";

import * as exerciseController from "../controllers/exercise.controller";
import { CreateExerciseDTO, FilterExerciseDTO, UpdateExerciseDTO } from "../services/exercise/exercise.dto";

const router = Router();

module.exports = async (app: Application) => {
  router.post("/", async (req: Request, res: Response) => {
    const payload: CreateExerciseDTO = req.body;
    const results = await exerciseController.create(payload);
    res.status(results.status).json(results.json);
  });

  router.put("/", async (req: Request, res: Response) => {
    const payload: UpdateExerciseDTO = req.body;
    const results = await exerciseController.update(payload);
    res.status(results.status).json(results.json);
  });

  router.get("/", async (req: Request, res: Response) => {
    const filters: FilterExerciseDTO = req.body;
    const results = await exerciseController.getAll(filters);
    res.status(results.status).json(results.json);
  });

  router.get("/:id", async (req: Request, res: Response) => {
    const filters: FilterExerciseDTO = req.body;
    const id: number = Number(req.params.id);
    const results = await exerciseController.getByIds([id], filters);
    res.status(results.status).json(results.json);
  });

  router.post("/equipments", async (req: Request, res: Response) => {
    const { ids, filters } = req.body;
    const results = await exerciseController.getWhereEquipmentIds(ids, filters);

    res.status(results.status).json(results.json);
  });

  router.post("/musclegroups", async (req: Request, res: Response) => {
    const { ids, filters } = req.body;
    const results = await exerciseController.getWhereMuscleGroupIds(ids, filters);

    res.status(results.status).json(results.json);
  });

  router.delete("/", async (req: Request, res: Response) => {
    const { ids, options } = req.body;

    const results = await exerciseController.deleteByIds(ids, options);
    res.status(results.status).json(results.json);
  });

  app.use("/api/exercise", router);
};
