import { Transaction } from "sequelize";
import validate from "validate.js";

// Models
import { sequelize } from "../models";
import Equipment from "../models/Equipment.model";
import { CreateEquipmentDTO, FilterEquipmentDTO, UpdateEquipmentDTO } from "../services/equipment/equipment.dto";

// Services
import * as service from "../services/equipment/equipment.service";

const contraints = {
  db_name: {
    presence: true,
    format: {
      pattern: "^[a-z0-9_-]+$",
      flags: "i",
      message: "^EQUIPMENT_DBNAME_INVALID",
    },
    length: {
      minimum: 6,
      tooShort: "^EQUIPMENT_DBNAME_TOO_SHORT",
    },
  },
};

export const create = async (payload: CreateEquipmentDTO): Promise<{ status: number; json: Equipment[] | {} | null }> => {
  let transaction: Transaction = await sequelize.transaction();
  const { db_name } = payload;

  let errors: { validator?: string; attributes: { [key: string]: string }; code: string }[] = [];

  const ex_founded = await service.getByDbNamesFactory([db_name]);

  if (ex_founded[0]) {
    errors.push({
      attributes: { db_name },
      code: "EQUIPMENT_DBNAME_ALREADY_TAKEN",
    });
  }

  const validation = validate({ db_name }, contraints, { format: "custom" });
  if (validation) errors.push(validation[0]);

  if (errors.length) return { status: 400, json: errors };

  try {
    const ex_created = await service.createFactory([{ db_name }], { transaction });
    if (transaction) transaction.commit();
    return { status: 200, json: ex_created.data };
  } catch (err) {
    if (transaction) transaction.rollback();
    return {
      status: 400,
      json: {
        attributes: [{ db_name }],
        code: "EQUIPMENT_CREATE_FAILED",
      },
    };
  }
};

export const update = async (payload: UpdateEquipmentDTO): Promise<{ status: number; json: Equipment[] | {} | null }> => {
  let transaction: Transaction = await sequelize.transaction();
  const { id, db_name } = payload;

  let errors: { validator?: string; attributes: { [key: string]: string }; code: string }[] = [];

  const ex_founded = await service.getByDbNamesFactory([db_name]);

  if (ex_founded[0] && ex_founded[0]?.id !== id) {
    errors.push({
      attributes: { db_name },
      code: "EQUIPMENT_DBNAME_ALREADY_TAKEN",
    });
  }

  const validation = validate({ db_name }, contraints, { format: "custom" });
  if (validation) errors.push(validation[0]);

  if (errors.length) return { status: 400, json: errors };

  try {
    const ex_updated = await service.updateFactory([payload], { transaction });
    if (transaction) transaction.commit();
    return { status: 201, json: ex_updated.data };
  } catch (err) {
    if (transaction) transaction.rollback();
    return {
      status: 400,
      json: {
        attributes: [{ db_name }],
        code: "EQUIPMENT_CREATE_FAILED",
      },
    };
  }
};

export const deleteByIds = async (payload: number[], options?: { force: boolean }): Promise<{ status: number; json: number | {} | null }> => {
  let transaction: Transaction = await sequelize.transaction();

  try {
    const ex_deleted = await service.deleteByIdsFactory(payload, { ...options, transaction });
    if (transaction) transaction.commit();
    return { status: 204, json: ex_deleted.data };
  } catch (err) {
    if (transaction) transaction.rollback();
    return {
      status: 400,
      json: {
        attributes: payload,
        code: "EQUIPMENT_DELETED_FAILED",
      },
    };
  }
};

export const getAll = async (filters: FilterEquipmentDTO): Promise<{ status: number; json: Equipment[] }> => {
  const results = await service.getAllFactory(undefined, filters);
  return { status: 200, json: results };
};

export const getByIds = async (payload: number[], filters: FilterEquipmentDTO): Promise<{ status: number; json: Equipment[] }> => {
  const results = await service.getByIdsFactory(payload, undefined, filters);
  return { status: 200, json: results };
};
