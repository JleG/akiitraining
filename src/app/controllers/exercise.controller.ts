import { Transaction } from "sequelize";
import validate from "validate.js";

// Models
import { sequelize } from "../models";
import Exercise from "../models/Exercise.model";
import { CreateExerciseDTO, FilterExerciseDTO, UpdateExerciseDTO } from "../services/exercise/exercise.dto";

// Services
import * as service from "../services/exercise/exercise.service";

const contraints = {
  db_name: {
    presence: true,
    format: {
      pattern: "^[a-z0-9_-]+$",
      flags: "i",
      message: "^EXERCISE_DBNAME_INVALID",
    },
    length: {
      minimum: 6,
      tooShort: "^EXERCISE_DBNAME_TOO_SHORT",
    },
  },
};

export const create = async (payload: CreateExerciseDTO): Promise<{ status: number; json: Exercise[] | {} | null }> => {
  let transaction: Transaction = await sequelize.transaction();
  const { db_name } = payload;

  let errors: { validator?: string; attributes: { [key: string]: string }; code: string }[] = [];

  const ex_founded = await service.getByDbNamesFactory([db_name]);

  if (ex_founded[0]) {
    errors.push({
      attributes: { db_name },
      code: "EXERCISE_DBNAME_ALREADY_TAKEN",
    });
  }

  const validation = validate({ db_name }, contraints, { format: "custom" });
  if (validation) errors.push(validation[0]);

  if (errors.length) return { status: 400, json: errors };

  try {
    const ex_created = await service.createFactory([{ db_name }], { transaction });
    if (transaction) transaction.commit();
    return { status: 200, json: ex_created.data };
  } catch (err) {
    if (transaction) transaction.rollback();
    return {
      status: 400,
      json: {
        attributes: [{ db_name }],
        code: "EXERCISE_CREATE_FAILED",
      },
    };
  }
};

export const update = async (payload: UpdateExerciseDTO): Promise<{ status: number; json: Exercise[] | {} | null }> => {
  let transaction: Transaction = await sequelize.transaction();
  const { id, db_name } = payload;

  let errors: { validator?: string; attributes: { [key: string]: string }; code: string }[] = [];

  const ex_founded = await service.getByDbNamesFactory([db_name]);

  if (ex_founded[0] && ex_founded[0]?.id !== id) {
    errors.push({
      attributes: { db_name },
      code: "EXERCISE_DBNAME_ALREADY_TAKEN",
    });
  }

  const validation = validate({ db_name }, contraints, { format: "custom" });
  if (validation) errors.push(validation[0]);

  if (errors.length) return { status: 400, json: errors };

  try {
    const ex_updated = await service.updateFactory([payload], { transaction });
    if (transaction) transaction.commit();
    return { status: 201, json: ex_updated.data };
  } catch (err) {
    if (transaction) transaction.rollback();
    return {
      status: 400,
      json: {
        attributes: [{ db_name }],
        code: "EXERCISE_CREATE_FAILED",
      },
    };
  }
};

export const deleteByIds = async (payload: number[], options?: { force: boolean }): Promise<{ status: number; json: number | {} | null }> => {
  let transaction: Transaction = await sequelize.transaction();

  try {
    const ex_deleted = await service.deleteByIdsFactory(payload, { ...options, transaction });
    if (transaction) transaction.commit();
    return { status: 204, json: ex_deleted.data };
  } catch (err) {
    if (transaction) transaction.rollback();
    return {
      status: 400,
      json: {
        attributes: payload,
        code: "EXERCISE_DELETED_FAILED",
      },
    };
  }
};

export const getAll = async (filters: FilterExerciseDTO): Promise<{ status: number; json: Exercise[] }> => {
  const results = await service.getAllFactory(undefined, filters);
  return { status: 200, json: results };
};

export const getByIds = async (payload: number[], filters: FilterExerciseDTO): Promise<{ status: number; json: Exercise[] }> => {
  const results = await service.getByIdsFactory(payload, undefined, filters);
  return { status: 200, json: results };
};

export const getWhereEquipmentIds = async (payload: number[], filters: FilterExerciseDTO): Promise<{ status: number; json: Exercise[] }> => {
  const results = await service.getWhereEquipmentIdsFactory(payload, undefined, filters);
  return { status: 200, json: results };
};

export const getWhereMuscleGroupIds = async (payload: number[], filters: FilterExerciseDTO): Promise<{ status: number; json: Exercise[] }> => {
  const results = await service.getWhereMuscleGroupIdsFactory(payload, undefined, filters);
  return { status: 200, json: results };
};
