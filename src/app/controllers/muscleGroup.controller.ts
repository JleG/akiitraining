import { Transaction } from "sequelize";
import validate from "validate.js";

// Models
import { sequelize } from "../models";
import MuscleGroup from "../models/MuscleGroup.model";
import { CreateMuscleGroupDTO, FilterMuscleGroupDTO, UpdateMuscleGroupDTO } from "../services/muscleGroup/muscleGroup.dto";

// Services
import * as service from "../services/muscleGroup/muscleGroup.service";

const contraints = {
  db_name: {
    presence: true,
    format: {
      pattern: "^[a-z0-9_-]+$",
      flags: "i",
      message: "^MUSCLE_GROUP_DBNAME_INVALID",
    },
    length: {
      minimum: 6,
      tooShort: "^MUSCLE_GROUP_DBNAME_TOO_SHORT",
    },
  },
};

export const create = async (payload: CreateMuscleGroupDTO): Promise<{ status: number; json: MuscleGroup[] | {} | null }> => {
  let transaction: Transaction = await sequelize.transaction();
  const { db_name } = payload;

  let errors: { validator?: string; attributes: { [key: string]: string }; code: string }[] = [];

  const mg_founded = await service.getByDbNamesFactory([db_name]);

  if (mg_founded[0]) {
    errors.push({
      attributes: { db_name },
      code: "MUSCLE_GROUP_DBNAME_ALREADY_TAKEN",
    });
  }

  const validation = validate({ db_name }, contraints, { format: "custom" });
  if (validation) errors.push(validation[0]);

  if (errors.length) return { status: 400, json: errors };

  try {
    const mg_created = await service.createFactory([{ db_name }], { transaction });
    if (transaction) transaction.commit();
    return { status: 200, json: mg_created.data };
  } catch (err) {
    if (transaction) transaction.rollback();
    return {
      status: 400,
      json: {
        attributes: [{ db_name }],
        code: "MUSCLE_GROUP_CREATE_FAILED",
      },
    };
  }
};

export const update = async (payload: UpdateMuscleGroupDTO): Promise<{ status: number; json: MuscleGroup[] | {} | null }> => {
  let transaction: Transaction = await sequelize.transaction();
  const { id, db_name } = payload;

  let errors: { validator?: string; attributes: { [key: string]: string }; code: string }[] = [];

  const mg_founded = await service.getByDbNamesFactory([db_name]);

  if (mg_founded[0] && mg_founded[0]?.id !== id) {
    errors.push({
      attributes: { db_name },
      code: "MUSCLE_GROUP_DBNAME_ALREADY_TAKEN",
    });
  }

  const validation = validate({ db_name }, contraints, { format: "custom" });
  if (validation) errors.push(validation[0]);

  if (errors.length) return { status: 400, json: errors };

  try {
    const mg_updated = await service.updateFactory([payload], { transaction });
    if (transaction) transaction.commit();
    return { status: 201, json: mg_updated.data };
  } catch (err) {
    if (transaction) transaction.rollback();
    return {
      status: 400,
      json: {
        attributes: [{ db_name }],
        code: "MUSCLE_GROUP_CREATE_FAILED",
      },
    };
  }
};

export const deleteByIds = async (payload: number[], options?: { force: boolean }): Promise<{ status: number; json: number | {} | null }> => {
  let transaction: Transaction = await sequelize.transaction();

  try {
    const mg_deleted = await service.deleteByIdsFactory(payload, { ...options, transaction });
    if (transaction) transaction.commit();
    return { status: 204, json: mg_deleted.data };
  } catch (err) {
    if (transaction) transaction.rollback();
    return {
      status: 400,
      json: {
        attributes: payload,
        code: "MUSCLE_GROUP_DELETED_FAILED",
      },
    };
  }
};

export const getAll = async (filters: FilterMuscleGroupDTO): Promise<{ status: number; json: MuscleGroup[] }> => {
  const results = await service.getAllFactory(undefined, filters);
  return { status: 200, json: results };
};

export const getByIds = async (payload: number[], filters: FilterMuscleGroupDTO): Promise<{ status: number; json: MuscleGroup[] }> => {
  const results = await service.getByIdsFactory(payload, undefined, filters);
  return { status: 200, json: results };
};
