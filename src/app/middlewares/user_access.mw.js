const db = require("../models");

exports.role_admin_access = (req, res, next) => {
  if (!req.user || req.user.role_level < 42) {
    return res.status(403).json({ error: "Forbidden access" });
  }
  next();
};

exports.role_user_access = (req, res, next) => {
  if (!req.user || req.user.role_level < 10) {
    return res.status(403).json({ error: "Forbidden access" });
  }
  next();
};
