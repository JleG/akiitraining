const bcrypt = require("bcrypt");
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const EthereumStrategy = require("passport-ethereum").Strategy;
const utils = require("ethereumjs-util");

const db = require("../models");

// Services
const Club = require("../services/clubs/club.service");
const Club_db = require("../services/clubs/club.db")(db);

// Functions
const findClubById = Club.findClubByIdFactory(Club_db);
const findClubByCredential = Club.findClubByCredentialFactory(Club_db);
const updateClub = Club.updateClubFactory(Club_db);

passport.serializeUser((club, done) => {
  done(null, club);
});

passport.deserializeUser((club, done) => {
  findClubById(club.id).then((club) => {
    done(null, club);
  });
});

passport.use(
  new EthereumStrategy((address, signature, done) => {
    findClubByCredential({ identifier: address })
      .then((club) => {
        const msg = `I signin to my club using the one-time use nonce: ${club.nonce}`;
        const msgBuffer = utils.toBuffer(msg);
        const msgHash = utils.hashPersonalMessage(msgBuffer);
        const signatureBuffer = utils.toBuffer(signature);
        const signatureParams = utils.fromRpcSig(signatureBuffer);
        const publicKey = utils.ecrecover(msgHash, signatureParams.v, signatureParams.r, signatureParams.s);
        const addressBuffer = utils.publicToAddress(publicKey);
        const public_address = utils.bufferToHex(addressBuffer);

        updateClub({
          where: { id: club.id },
          next: { ...club.dataValues, nonce: Math.floor(Math.random() * 1000000) },
        });

        if (address.toLowerCase() === public_address.toLowerCase()) {
          return done(null, club);
        } else {
          return done(null, false);
        }
      })
      .catch((err) => {
        return done(err);
      });
    // return done(null, true);
  })
);

passport.use(
  new LocalStrategy(
    {
      clubnameField: "identifier",
    },
    (clubname, password, done) => {
      findClubByCredential({ identifier: clubname, password })
        .then((club) => {
          if (club && !_validPassword(club.hashedPassword, password)) {
            return done(null, false);
          } else {
            return done(null, club);
          }
        })
        .catch((err) => {
          return done(err);
        });
    }
  )
);

// Checks password validity
const _validPassword = (hashedPassword, password) => {
  return bcrypt.compareSync(password, hashedPassword);
};
