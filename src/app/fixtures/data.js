import { Equipment, MuscleGroup, Exercise } from "../models";

const equipmentData = [{ name: "Barbell" }, { name: "Dumbbell" }, { name: "Kettlebell" }, { name: "Bench" }, { name: "Pull up bar" }];

const muscleGroupData = [{ name: "Chest" }, { name: "Back" }, { name: "Shoulders" }, { name: "Biceps" }, { name: "Triceps" }, { name: "Legs" }];

const exerciseData = [
  { name: "Barbell Bench Press", description: "A classic chest exercise using a barbell." },
  { name: "Dumbbell Flys", description: "An isolation exercise for the chest using dumbbells." },
  { name: "Pull Ups", description: "A back exercise using a pull up bar." },
  { name: "Barbell Bicep Curl", description: "A bicep exercise using a barbell." },
  { name: "Dumbbell Tricep Extension", description: "A tricep exercise using a dumbbell." },
  { name: "Barbell Squat", description: "A leg exercise using a barbell." },
];

const seedData = async () => {
  const [barbell, dumbbell, kettlebell, bench, pullUpBar] = await Equipment.bulkCreate(equipmentData, { returning: true });
  const [chest, back, shoulders, biceps, triceps, legs] = await MuscleGroup.bulkCreate(muscleGroupData, { returning: true });
  const [barbellBenchPress, dumbbellFlys, pullUps, barbellBicepCurl, dumbbellTricepExtension, barbellSquat] = await Exercise.bulkCreate(exerciseData, { returning: true });

  await barbellBenchPress.setEquipment([barbell, bench]);
  await barbellBenchPress.setMuscleGroups([chest]);
  await dumbbellFlys.setEquipment([dumbbell]);
  await dumbbellFlys.setMuscleGroups([chest]);
  await pullUps.setEquipment([pullUpBar]);
  await pullUps.setMuscleGroups([back]);
  await barbellBicepCurl.setEquipment([barbell]);
  await barbellBicepCurl.setMuscleGroups([biceps]);
  await dumbbellTricepExtension.setEquipment([dumbbell]);
  await dumbbellTricepExtension.setMuscleGroups([triceps]);
  await barbellSquat.setEquipment([barbell]);
  await barbellSquat.setMuscleGroups([legs]);
};

const data = await seedData();
console.log({ data });
