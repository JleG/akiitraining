import { expect } from "chai";
const { validateEquipmentExistsFactory, createFactory, updateFactory, deleteFactory, deleteByIdsFactory, getAllFactory, getByIdsFactory, getByDbNamesFactory } = require("../../services/equipment/equipment.service");

describe("Equipment Service Test Suite", () => {
  it("Should see if Equipment already exists on a non-existing Equipment", async () => {
    const result = await validateEquipmentExistsFactory("ut_dbname");
    expect(result).to.be.empty;
  });
  it("Should see if a Equipment already exists on an existing db_name", async () => {
    const result = await validateEquipmentExistsFactory("ut_dbname");
    expect(result).to.be.an("array");
  });
  it("Should throw an error because no args was passed to see if Equipment exists", async () => {
    try {
      await validateEquipmentExistsFactory();
    } catch (e: any) {
      expect(e).to.be.an("Error");
      expect(e.message).to.eq("Invalid number of args, please pass a db_name.");
    }
  });

  it("Should create a new Equipment", async () => {
    const result = await createFactory([]);
    result.transaction.rollback();
    expect(result).to.be.an("object");
    expect(result.data).to.be.an("array");
  });

  it("Should update a Equipment", async () => {
    const result = await updateFactory([]);
    result.transaction.rollback();
    expect(result).to.be.an("object");
    expect(result.data).to.be.an("array");
  });

  it("Should delete a Equipment", async () => {
    const result = await deleteFactory([{ id: -1 }]);
    result.transaction.rollback();
    expect(result).to.be.an("object");
    expect(result.data).to.eq(null);
  });

  it("Should delete by ids a Equipment", async () => {
    const result = await deleteByIdsFactory([{ id: -1 }]);
    result.transaction.rollback();
    expect(result).to.be.an("object");
    expect(result.data).to.eq(null);
  });

  it("Should return a list of Equipment", async () => {
    const result = await getAllFactory();
    expect(result).to.be.an("array");
  });

  it("Should return a list of Equipment based on id", async () => {
    const result = await getByIdsFactory([-1]);
    expect(result).to.be.an("array");
  });

  it("Should return a list of Equipment based on db_name", async () => {
    const result = await getByDbNamesFactory(["ut_dbname"]);
    expect(result).to.be.an("array");
  });
});
