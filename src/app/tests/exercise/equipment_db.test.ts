import { expect } from "chai";
import { Transaction } from "sequelize";
import { sequelize } from "../../models";
import * as Equipment_db from "../../services/equipment/equipment.db";
import Equipment, { EquipmentInput, EquipmentOutput } from "../../models/Equipment.model";

describe("Equipment DB Test Suite", () => {
  describe("Equipment Create Suite", () => {
    describe("Should create a new equipment", () => {
      it("When all minimum arguments are provided", async () => {
        const db_name = "ut_dbname";
        const ut_equipment_created = await Equipment_db.create([{ db_name }]);
        await ut_equipment_created.transaction.rollback();

        expect(ut_equipment_created.data).to.be.an("array");
        expect(ut_equipment_created.transaction).to.be.an.instanceof(Transaction);
        for (const key in ut_equipment_created.data) {
          expect(ut_equipment_created.data?.[eval(key)]).to.be.an.instanceof(Equipment);
          expect(ut_equipment_created.data?.[eval(key)].db_name).to.eq(db_name);
        }
      });
    });
    describe("Should create multiple new equipment", () => {
      it("When all minimum arguments are provided", async () => {
        const db_name1 = "ut_dbname1";
        const db_name2 = "ut_dbname2";
        const ut_equipment_created = await Equipment_db.create([{ db_name: db_name1 }, { db_name: db_name2 }]);
        await ut_equipment_created.transaction.rollback();

        expect(ut_equipment_created.data).to.be.an("array");
        expect(ut_equipment_created.transaction).to.be.an.instanceof(Transaction);
        for (const key in ut_equipment_created.data) {
          expect(ut_equipment_created.data?.[eval(key)]).to.be.an.instanceof(Equipment);
          expect(ut_equipment_created.data?.[eval(key)].db_name).to.eq(eval(`db_name${parseInt(key) + 1}`));
        }
      });
    });
    describe("Should trow an error", () => {
      it("When no db_name was provided", async () => {
        try {
          await Equipment_db.create([{}]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("Invalid argument: db_name");
        }
      });
      it("When db_name is empty", async () => {
        try {
          const db_name = "";
          await Equipment_db.create([{ db_name }]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("Invalid argument: db_name");
        }
      });
      it("When db_name already exist", async () => {
        const dummy_equipment = await _createDummyEquipment(1);
        try {
          await Equipment_db.create([{ db_name: dummy_equipment.data[0].db_name }], { transaction: dummy_equipment.transaction });
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq(`Invalid argument: db_name "${dummy_equipment.data[0].db_name}" already exists.`);
        } finally {
          dummy_equipment.transaction.rollback();
        }
      });
      it("When the same db_name is inserted several times", async () => {
        try {
          const ut_muscul_group_created = await Equipment_db.create([{ db_name: "ut_dbname" }, { db_name: "ut_dbname" }]);
          ut_muscul_group_created.transaction.rollback();
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq(`Invalid argument: db_name`);
        }
      });
      it("When the transaction rollback", async () => {
        const ut_equipment = await Equipment_db.create([
          { id: 1, db_name: "ut_dbname1" },
          { id: 1, db_name: "ut_dbname2" },
        ]);
        ut_equipment.transaction.rollback();

        expect(ut_equipment).to.be.an("object");
        expect(ut_equipment.data).to.be.null;
        expect(ut_equipment.transaction).to.be.an.instanceof(Transaction);
      });
    });
  });

  describe("Equipment Upsert Suite", () => {
    describe("Should upsert a new equipment", () => {
      it("When all minimum arguments are provided", async () => {
        const db_name = "ut_dbname";
        const ut_equipment_upserted = await Equipment_db.upsert([{ db_name }]);
        await ut_equipment_upserted.transaction.rollback();

        expect(ut_equipment_upserted.data).to.be.an("array");
        expect(ut_equipment_upserted.transaction).to.be.an.instanceof(Transaction);
        for (const key in ut_equipment_upserted.data) {
          expect(ut_equipment_upserted.data?.[eval(key)]).to.be.an.instanceof(Equipment);
          expect(ut_equipment_upserted.data?.[eval(key)].db_name).to.eq(db_name);
        }
      });
    });
    describe("Should upsert multiple new equipment", () => {
      it("When all minimum arguments are provided", async () => {
        const db_name1 = "ut_dbname1";
        const db_name2 = "ut_dbname2";
        const ut_equipment_upserted = await Equipment_db.upsert([{ db_name: db_name1 }, { db_name: db_name2 }]);
        await ut_equipment_upserted.transaction.rollback();

        expect(ut_equipment_upserted.data).to.be.an("array");
        expect(ut_equipment_upserted.transaction).to.be.an.instanceof(Transaction);
        for (const key in ut_equipment_upserted.data) {
          expect(ut_equipment_upserted.data?.[eval(key)]).to.be.an.instanceof(Equipment);
          expect(ut_equipment_upserted.data?.[eval(key)].db_name).to.eq(eval(`db_name${parseInt(key) + 1}`));
        }
      });
    });
    describe("Should trow an error", () => {
      it("When no db_name was provided", async () => {
        try {
          await Equipment_db.upsert([{}]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("Invalid argument: db_name");
        }
      });
      it("When db_name is empty", async () => {
        try {
          const db_name = "";
          await Equipment_db.upsert([{ db_name }]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("Invalid argument: db_name");
        }
      });
      it("When db_name already exist", async () => {
        const dummy_equipment = await _createDummyEquipment(1);
        try {
          await Equipment_db.upsert([{ db_name: dummy_equipment.data[0].db_name }], { transaction: dummy_equipment.transaction });
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq(`Invalid argument: db_name "${dummy_equipment.data[0].db_name}" already exists.`);
        } finally {
          dummy_equipment.transaction.rollback();
        }
      });
      it("When the same db_name is inserted several times", async () => {
        try {
          const ut_muscul_group_upserted = await Equipment_db.upsert([{ db_name: "ut_dbname" }, { db_name: "ut_dbname" }]);
          ut_muscul_group_upserted.transaction.rollback();
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq(`Invalid argument: db_name`);
        }
      });
      // Can't find a solution for generate an rollback test.
      // it("When the transaction rollback", async () => {
      //   const ut_equipment_upserted = await Equipment_db.upsert([{ id: -1, db_name: "" }]);
      //   ut_equipment_upserted.transaction.rollback();

      //   expect(ut_equipment_upserted).to.be.an("object");
      //   expect(ut_equipment_upserted.data).to.be.null;
      //   expect(ut_equipment_upserted.transaction).to.be.an.instanceof(Transaction);
      // });
    });
  });

  describe("Equipment Update Suite", () => {
    describe("Should update a equipment", () => {
      it("When new db_name argument is updated", async () => {
        const dummy_equipment = await _createDummyEquipment(1);
        dummy_equipment.data[0].db_name = "ut_next_dbname";
        const ut_equipment_updated = await Equipment_db.update([dummy_equipment.data[0].dataValues], { transaction: dummy_equipment.transaction });
        ut_equipment_updated.transaction.rollback();

        expect(ut_equipment_updated.data).to.be.an("array");
        expect(ut_equipment_updated.transaction).to.be.an.instanceof(Transaction);
        for (const key in ut_equipment_updated.data) {
          expect(ut_equipment_updated.data?.[eval(key)]).to.be.an.instanceof(Equipment);
          expect(ut_equipment_updated.data?.[eval(key)].db_name).to.eq("ut_next_dbname");
        }
      });
    });
    describe("Should update multiple equipment", () => {
      it("When new db_name argument is updated", async () => {
        const dummy_equipment = await _createDummyEquipment(2);

        const ut_equipment_to_update: { id: number; db_name: string }[] = [];
        for (const key in dummy_equipment.data) {
          const tmp = { ...dummy_equipment.data[key].dataValues };
          tmp.db_name = "ut_next_dbname" + key;
          ut_equipment_to_update.push(tmp);
        }

        const ut_equipment_updated = await Equipment_db.update(ut_equipment_to_update, { transaction: dummy_equipment.transaction });
        ut_equipment_updated.transaction.rollback();

        for (const key in ut_equipment_updated.data) {
          expect(ut_equipment_updated.data[eval(key)]).to.be.an.instanceof(Equipment);
          expect(ut_equipment_updated.data[eval(key)].db_name).to.eq(`ut_next_dbname${key}`);
        }
      });
    });
    describe("Should trow an error", () => {
      it("When db_name is empty", async () => {
        const dummy_equipment = await _createDummyEquipment(2);

        const ut_equipment_to_update: { id: number; db_name: string }[] = [];
        for (const key in dummy_equipment.data) {
          const tmp = { ...dummy_equipment.data[eval(key)].dataValues };
          tmp.db_name = "";
          ut_equipment_to_update.push(tmp);
        }

        try {
          await Equipment_db.update(ut_equipment_to_update, { transaction: dummy_equipment.transaction });
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq(`Invalid argument: db_name`);
        } finally {
          dummy_equipment.transaction.rollback();
        }
      });
      it("When db_name already exist", async () => {
        const dummy_equipment = await _createDummyEquipment(2);

        const ut_equipment_to_update: { id: number; db_name: string }[] = [];
        for (const key in dummy_equipment.data) {
          const tmp = { ...dummy_equipment.data[key].dataValues };
          tmp.db_name = dummy_equipment.data[0].db_name;
          ut_equipment_to_update.push(tmp);
        }

        try {
          await Equipment_db.update(ut_equipment_to_update, { transaction: dummy_equipment.transaction });
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq(`Invalid argument: db_name "${dummy_equipment.data[0].db_name}" already exists.`);
        } finally {
          dummy_equipment.transaction.rollback();
        }
      });

      it("When the transaction rollback", async () => {
        const dummy_equipment = await _createDummyEquipment(2);

        const ut_equipment_to_update: { id: number; db_name: string }[] = [];
        for (const key in dummy_equipment.data) {
          const tmp = { ...dummy_equipment.data[key].dataValues };
          tmp.db_name = "ut_dbname";
          ut_equipment_to_update.push(tmp);
        }

        const ut_equipment_updated = await Equipment_db.update(ut_equipment_to_update, { transaction: dummy_equipment.transaction });

        dummy_equipment.transaction.rollback();

        expect(ut_equipment_updated).to.be.an("object");
        expect(ut_equipment_updated.data).to.be.null;
        expect(ut_equipment_updated.transaction).to.be.an.instanceof(Transaction);
      });
    });
  });

  describe("Equipment Delete Suite", () => {
    describe("Should delete a equipment ", () => {
      it("When a Equipment object arguments is provided", async () => {
        const dummy_equipment = await _createDummyEquipment(1);
        const ut_equipment_deleted = await Equipment_db.deleteByObject(dummy_equipment.data, { force: true, transaction: dummy_equipment.transaction });
        dummy_equipment.transaction.rollback();
        expect(ut_equipment_deleted.data).to.eq(1);
      });
      it("When an id arguments is provided", async () => {
        const dummy_equipment = await _createDummyEquipment(1);
        const ut_equipment_deleted = await Equipment_db.deleteByIds([dummy_equipment.data[0].id], { force: true, transaction: dummy_equipment.transaction });
        dummy_equipment.transaction.rollback();
        expect(ut_equipment_deleted.data).to.eq(1);
      });
    });
    describe("Should delete multiple equipment ", () => {
      it("When multiple Equipment objects arguments are provided", async () => {
        const dummy_equipment = await _createDummyEquipment(5);
        const ut_equipment_deleted = await Equipment_db.deleteByObject(dummy_equipment.data, { force: true, transaction: dummy_equipment.transaction });
        dummy_equipment.transaction.rollback();
        expect(ut_equipment_deleted.data).to.eq(5);
      });
      it("When multiple ids arguments are provided", async () => {
        const dummy_equipment = await _createDummyEquipment(5);
        const id_list: number[] = [];
        for (const mg of dummy_equipment.data) {
          const { id } = mg;
          id_list.push(id);
        }
        const ut_equipment_deleted = await Equipment_db.deleteByIds(id_list, { force: true, transaction: dummy_equipment.transaction });
        dummy_equipment.transaction.rollback();
        expect(ut_equipment_deleted.data).to.eq(5);
      });
    });
    describe("Should trow an error", () => {
      it("When no Equipment object was provided", async () => {
        try {
          await Equipment_db.deleteByObject([]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("No Equipment object was provided as an argument on deletion.");
        }
      });
      it("When Equipment object provided has no id", async () => {
        try {
          await Equipment_db.deleteByObject([{ db_name: "ut_dbname" }]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("Invalid argument: id");
        }
      });
      it("When Equipment object provided does not exist", async () => {
        try {
          await Equipment_db.deleteByObject([{ id: -1, db_name: "ut_dbname" }]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("Error when deleting one or more Equipment object(s).");
        }
      });
      it("When no ids was provided", async () => {
        try {
          await Equipment_db.deleteByIds([]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("No ids was provided as an argument on deletion.");
        }
      });
      it("When ids provided does not exist", async () => {
        try {
          await Equipment_db.deleteByIds([-1]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq(`Error when deleting one or more Equipment object(s) with id list.`);
        }
      });
    });
  });

  describe("Equipment Finding Suite", () => {
    describe("Should return equipment", () => {
      it("When trying to get all without filter", async () => {
        const dummy_equipment = await _createDummyEquipment(5);
        const dummy_length = dummy_equipment.data.length;
        const ut_equipment_founded = await Equipment_db.getAll(dummy_equipment.transaction);
        dummy_equipment.transaction.rollback();

        expect(ut_equipment_founded).to.be.an("array");
        // expect(ut_equipment_founded).to.have.lengthOf(dummy_length - 1);
        for (const equipment of dummy_equipment.data) {
          expect(equipment).to.be.instanceOf(Equipment);
        }
      });
      it("When trying to get all with filter isDelete", async () => {
        const dummy_equipment = await _createDummyEquipment(5);
        const ut_equipment_founded = await Equipment_db.getAll(dummy_equipment.transaction, { isDelete: true });
        dummy_equipment.transaction.rollback();

        expect(ut_equipment_founded).to.be.an("array");
        for (const equipment of dummy_equipment.data) {
          expect(equipment).to.be.instanceOf(Equipment);
        }
      });
      it("When trying to get all with filter includeDeleted", async () => {
        const dummy_equipment = await _createDummyEquipment(5);
        const ut_equipment_founded = await Equipment_db.getAll(dummy_equipment.transaction, { includeDeleted: true });
        dummy_equipment.transaction.rollback();

        expect(ut_equipment_founded).to.be.an("array");
        for (const equipment of dummy_equipment.data) {
          expect(equipment).to.be.instanceOf(Equipment);
        }
      });
      it("When his id was provided without filter", async () => {
        const dummy_equipment = await _createDummyEquipment(1);
        const ut_equipment_founded = await Equipment_db.getByIds([dummy_equipment.data[0].id], { transaction: dummy_equipment.transaction });
        dummy_equipment.transaction.rollback();

        expect(ut_equipment_founded[0]).to.be.an.instanceof(Equipment);
        expect(ut_equipment_founded[0].id).to.eq(dummy_equipment.data[0].id);
      });
      it("When his id was provided with filter isDelete", async () => {
        const dummy_equipment = await _createDummyEquipment(5);
        const ut_equipment_founded = await Equipment_db.getByIds([dummy_equipment.data[0].id], { transaction: dummy_equipment.transaction }, { isDelete: true });
        dummy_equipment.transaction.rollback();

        expect(ut_equipment_founded).to.be.an("array");
        for (const equipment of dummy_equipment.data) {
          expect(equipment).to.be.instanceOf(Equipment);
        }
      });
      it("When his id was provided with filter includeDeleted", async () => {
        const dummy_equipment = await _createDummyEquipment(5);
        const ut_equipment_founded = await Equipment_db.getByIds([dummy_equipment.data[0].id], { transaction: dummy_equipment.transaction }, { includeDeleted: true });
        dummy_equipment.transaction.rollback();

        expect(ut_equipment_founded).to.be.an("array");
        for (const equipment of dummy_equipment.data) {
          expect(equipment).to.be.instanceOf(Equipment);
        }
      });
      it("When his db_name was provided", async () => {
        const dummy_equipment = await _createDummyEquipment(1);
        const ut_equipment_founded = await Equipment_db.getByDbNames([dummy_equipment.data[0].db_name], { transaction: dummy_equipment.transaction });
        dummy_equipment.transaction.rollback();

        expect(ut_equipment_founded[0]).to.be.an.instanceof(Equipment);
        expect(ut_equipment_founded[0].db_name).to.eq(dummy_equipment.data[0].db_name);
      });
      it("When his db_name was provided with filter isDelete", async () => {
        const dummy_equipment = await _createDummyEquipment(5);
        const ut_equipment_founded = await Equipment_db.getByDbNames([dummy_equipment.data[0].db_name], { transaction: dummy_equipment.transaction }, { isDelete: true });
        dummy_equipment.transaction.rollback();

        expect(ut_equipment_founded).to.be.an("array");
        for (const equipment of dummy_equipment.data) {
          expect(equipment).to.be.instanceOf(Equipment);
        }
      });
      it("When his db_name was provided with filter includeDeleted", async () => {
        const dummy_equipment = await _createDummyEquipment(5);
        const ut_equipment_founded = await Equipment_db.getByDbNames([dummy_equipment.data[0].db_name], { transaction: dummy_equipment.transaction }, { includeDeleted: true });
        dummy_equipment.transaction.rollback();

        expect(ut_equipment_founded).to.be.an("array");
        for (const equipment of dummy_equipment.data) {
          expect(equipment).to.be.instanceOf(Equipment);
        }
      });
      it("When multiple ids was provided", async () => {
        const dummy_equipment = await _createDummyEquipment(2);
        const ut_equipment_founded = await Equipment_db.getByIds([dummy_equipment.data[0].id, dummy_equipment.data[1].id], { transaction: dummy_equipment.transaction });
        dummy_equipment.transaction.rollback();

        expect(ut_equipment_founded).to.be.an("array");
        for (const key in dummy_equipment.data) {
          expect(ut_equipment_founded[eval(key)]).to.be.an.instanceof(Equipment);
          expect(ut_equipment_founded[eval(key)].id).to.eq(dummy_equipment.data[eval(key)].id);
        }
      });
      it("When multiple db_name was provided", async () => {
        const dummy_equipment = await _createDummyEquipment(2);
        const ut_equipment_founded = await Equipment_db.getByDbNames([dummy_equipment.data[0].db_name, dummy_equipment.data[1].db_name], { transaction: dummy_equipment.transaction });
        dummy_equipment.transaction.rollback();

        expect(ut_equipment_founded).to.be.an("array");
        for (const key in dummy_equipment.data) {
          expect(ut_equipment_founded[eval(key)]).to.be.an.instanceof(Equipment);
          expect(ut_equipment_founded[eval(key)].db_name).to.eq(dummy_equipment.data[eval(key)].db_name);
        }
      });
      describe("Should throw an error", () => {
        it("When no ids was provided", async () => {
          try {
            await Equipment_db.getByIds([]);
          } catch (e: any) {
            expect(e).to.be.an("Error");
            expect(e.message).to.eq("No id was provided as an argument on finding.");
          }
        });
        it("When no db_name was provided", async () => {
          try {
            await Equipment_db.getByDbNames([]);
          } catch (e: any) {
            expect(e).to.be.an("Error");
            expect(e.message).to.eq("No db_name was provided as an argument on finding.");
          }
        });
      });
    });
  });
});

// /** Create a Dummy Equipment */
const _createDummyEquipment = async (number: number): Promise<{ data: Equipment[]; transaction: Transaction }> => {
  const dummy_list: { db_name: string }[] = [];
  for (let i = 0; i < number; i++) {
    dummy_list.push({ db_name: `dummy_dbname_${i}` });
  }

  let transaction = await sequelize.transaction();
  const dummy_muscule_group_upderted = await Equipment.bulkCreate(dummy_list, { transaction });

  return { data: dummy_muscule_group_upderted, transaction };
};

// /** Destroy the Dummy equipment */
// const _destroyDummyEquipment = async (dummy_equipment: Equipment | Equipment[], options?: { force?: boolean; transaction?: Transaction }) => {
//   const force = options?.force ? options.force : false;
//   const transaction = options?.transaction ? options.transaction : await sequelize.transaction();

//   if (Array.isArray(dummy_equipment)) {
//     const id_list: number[] = [];
//     for (const mg of dummy_equipment) {
//       const { id } = mg;
//       id_list.push(id);
//     }
//     return await Equipment.destroy({ where: { id: id_list }, force, transaction });
//   } else return await dummy_equipment.destroy({ force, transaction });
// };
