import { expect } from "chai";
const { validateMuscleGroupExistsFactory, createFactory, updateFactory, deleteFactory, deleteByIdsFactory, getAllFactory, getByIdsFactory, getByDbNamesFactory } = require("../../services/muscleGroup/muscleGroup.service");

describe("MuscleGroup Service Test Suite", () => {
  it("Should see if MuscleGroup already exists on a non-existing MuscleGroup", async () => {
    const result = await validateMuscleGroupExistsFactory("ut_dbname");
    expect(result).to.be.empty;
  });
  it("Should see if a MuscleGroup already exists on an existing db_name", async () => {
    const result = await validateMuscleGroupExistsFactory("ut_dbname");
    expect(result).to.be.an("array");
  });
  it("Should throw an error because no args was passed to see if MuscleGroup exists", async () => {
    try {
      await validateMuscleGroupExistsFactory();
    } catch (e: any) {
      expect(e).to.be.an("Error");
      expect(e.message).to.eq("Invalid number of args, please pass a db_name.");
    }
  });

  it("Should create a new MuscleGroup", async () => {
    const result = await createFactory([]);
    result.transaction.rollback();
    expect(result).to.be.an("object");
    expect(result.data).to.be.an("array");
  });

  it("Should update a MuscleGroup", async () => {
    const result = await updateFactory([]);
    result.transaction.rollback();
    expect(result).to.be.an("object");
    expect(result.data).to.be.an("array");
  });

  it("Should delete a MuscleGroup", async () => {
    const result = await deleteFactory([{ id: -1 }]);
    result.transaction.rollback();
    expect(result).to.be.an("object");
    expect(result.data).to.eq(null);
  });

  it("Should delete by ids a MuscleGroup", async () => {
    const result = await deleteByIdsFactory([{ id: -1 }]);
    result.transaction.rollback();
    expect(result).to.be.an("object");
    expect(result.data).to.eq(null);
  });

  it("Should return a list of MuscleGroup", async () => {
    const result = await getAllFactory();
    expect(result).to.be.an("array");
  });

  it("Should return a list of MuscleGroup based on id", async () => {
    const result = await getByIdsFactory([-1]);
    expect(result).to.be.an("array");
  });

  it("Should return a list of MuscleGroup based on db_name", async () => {
    const result = await getByDbNamesFactory(["ut_dbname"]);
    expect(result).to.be.an("array");
  });
});
