import { expect } from "chai";
import { Transaction } from "sequelize";
import { sequelize } from "../../models";
import * as MuscleGroup_db from "../../services/muscleGroup/muscleGroup.db";
import MuscleGroup, { MuscleGroupInput, MuscleGroupOutput } from "../../models/MuscleGroup.model";

describe("MuscleGroup DB Test Suite", () => {
  describe("MuscleGroup Create Suite", () => {
    describe("Should create a new muscle group", () => {
      it("When all minimum arguments are provided", async () => {
        const db_name = "ut_dbname";
        const ut_muscle_group_created = await MuscleGroup_db.create([{ db_name }]);
        await ut_muscle_group_created.transaction.rollback();

        expect(ut_muscle_group_created.data).to.be.an("array");
        expect(ut_muscle_group_created.transaction).to.be.an.instanceof(Transaction);
        for (const key in ut_muscle_group_created.data) {
          expect(ut_muscle_group_created.data?.[eval(key)]).to.be.an.instanceof(MuscleGroup);
          expect(ut_muscle_group_created.data?.[eval(key)].db_name).to.eq(db_name);
        }
      });
    });
    describe("Should create multiple new muscle group", () => {
      it("When all minimum arguments are provided", async () => {
        const db_name1 = "ut_dbname1";
        const db_name2 = "ut_dbname2";
        const ut_muscle_group_created = await MuscleGroup_db.create([{ db_name: db_name1 }, { db_name: db_name2 }]);
        await ut_muscle_group_created.transaction.rollback();

        expect(ut_muscle_group_created.data).to.be.an("array");
        expect(ut_muscle_group_created.transaction).to.be.an.instanceof(Transaction);
        for (const key in ut_muscle_group_created.data) {
          expect(ut_muscle_group_created.data?.[eval(key)]).to.be.an.instanceof(MuscleGroup);
          expect(ut_muscle_group_created.data?.[eval(key)].db_name).to.eq(eval(`db_name${parseInt(key) + 1}`));
        }
      });
    });
    describe("Should trow an error", () => {
      it("When no db_name was provided", async () => {
        try {
          await MuscleGroup_db.create([{}]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("Invalid argument: db_name");
        }
      });
      it("When db_name is empty", async () => {
        try {
          const db_name = "";
          await MuscleGroup_db.create([{ db_name }]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("Invalid argument: db_name");
        }
      });
      it("When db_name already exist", async () => {
        const dummy_muscle_group = await _createDummyMuscleGroup(1);
        try {
          await MuscleGroup_db.create([{ db_name: dummy_muscle_group.data[0].db_name }], { transaction: dummy_muscle_group.transaction });
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq(`Invalid argument: db_name "${dummy_muscle_group.data[0].db_name}" already exists.`);
        } finally {
          dummy_muscle_group.transaction.rollback();
        }
      });
      it("When the same db_name is inserted several times", async () => {
        try {
          const ut_muscul_group_created = await MuscleGroup_db.create([{ db_name: "ut_dbname" }, { db_name: "ut_dbname" }]);
          ut_muscul_group_created.transaction.rollback();
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq(`Invalid argument: db_name`);
        }
      });
      it("When the transaction rollback", async () => {
        const ut_muscle_group = await MuscleGroup_db.create([
          { id: 1, db_name: "ut_dbname1" },
          { id: 1, db_name: "ut_dbname2" },
        ]);
        ut_muscle_group.transaction.rollback();

        expect(ut_muscle_group).to.be.an("object");
        expect(ut_muscle_group.data).to.be.null;
        expect(ut_muscle_group.transaction).to.be.an.instanceof(Transaction);
      });
    });
  });

  describe("MuscleGroup Upsert Suite", () => {
    describe("Should upsert a new muscle group", () => {
      it("When all minimum arguments are provided", async () => {
        const db_name = "ut_dbname";
        const ut_muscle_group_upserted = await MuscleGroup_db.upsert([{ db_name }]);
        await ut_muscle_group_upserted.transaction.rollback();

        expect(ut_muscle_group_upserted.data).to.be.an("array");
        expect(ut_muscle_group_upserted.transaction).to.be.an.instanceof(Transaction);
        for (const key in ut_muscle_group_upserted.data) {
          expect(ut_muscle_group_upserted.data?.[eval(key)]).to.be.an.instanceof(MuscleGroup);
          expect(ut_muscle_group_upserted.data?.[eval(key)].db_name).to.eq(db_name);
        }
      });
    });
    describe("Should upsert multiple new muscle group", () => {
      it("When all minimum arguments are provided", async () => {
        const db_name1 = "ut_dbname1";
        const db_name2 = "ut_dbname2";
        const ut_muscle_group_upserted = await MuscleGroup_db.upsert([{ db_name: db_name1 }, { db_name: db_name2 }]);
        await ut_muscle_group_upserted.transaction.rollback();

        expect(ut_muscle_group_upserted.data).to.be.an("array");
        expect(ut_muscle_group_upserted.transaction).to.be.an.instanceof(Transaction);
        for (const key in ut_muscle_group_upserted.data) {
          expect(ut_muscle_group_upserted.data?.[eval(key)]).to.be.an.instanceof(MuscleGroup);
          expect(ut_muscle_group_upserted.data?.[eval(key)].db_name).to.eq(eval(`db_name${parseInt(key) + 1}`));
        }
      });
    });
    describe("Should trow an error", () => {
      it("When no db_name was provided", async () => {
        try {
          await MuscleGroup_db.upsert([{}]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("Invalid argument: db_name");
        }
      });
      it("When db_name is empty", async () => {
        try {
          const db_name = "";
          await MuscleGroup_db.upsert([{ db_name }]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("Invalid argument: db_name");
        }
      });
      it("When db_name already exist", async () => {
        const dummy_muscle_group = await _createDummyMuscleGroup(1);
        try {
          await MuscleGroup_db.upsert([{ db_name: dummy_muscle_group.data[0].db_name }], { transaction: dummy_muscle_group.transaction });
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq(`Invalid argument: db_name "${dummy_muscle_group.data[0].db_name}" already exists.`);
        } finally {
          dummy_muscle_group.transaction.rollback();
        }
      });
      it("When the same db_name is inserted several times", async () => {
        try {
          const ut_muscul_group_upserted = await MuscleGroup_db.upsert([{ db_name: "ut_dbname" }, { db_name: "ut_dbname" }]);
          ut_muscul_group_upserted.transaction.rollback();
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq(`Invalid argument: db_name`);
        }
      });
      // Can't find a solution for generate an rollback test.
      // it("When the transaction rollback", async () => {
      //   const ut_muscle_group_upserted = await MuscleGroup_db.upsert([{ id: -1, db_name: "" }]);
      //   ut_muscle_group_upserted.transaction.rollback();

      //   expect(ut_muscle_group_upserted).to.be.an("object");
      //   expect(ut_muscle_group_upserted.data).to.be.null;
      //   expect(ut_muscle_group_upserted.transaction).to.be.an.instanceof(Transaction);
      // });
    });
  });

  describe("MuscleGroup Update Suite", () => {
    describe("Should update a muscle group", () => {
      it("When new db_name argument is updated", async () => {
        const dummy_muscle_group = await _createDummyMuscleGroup(1);
        dummy_muscle_group.data[0].db_name = "ut_next_dbname";
        const ut_muscle_group_updated = await MuscleGroup_db.update([dummy_muscle_group.data[0].dataValues], { transaction: dummy_muscle_group.transaction });
        ut_muscle_group_updated.transaction.rollback();

        expect(ut_muscle_group_updated.data).to.be.an("array");
        expect(ut_muscle_group_updated.transaction).to.be.an.instanceof(Transaction);
        for (const key in ut_muscle_group_updated.data) {
          expect(ut_muscle_group_updated.data?.[eval(key)]).to.be.an.instanceof(MuscleGroup);
          expect(ut_muscle_group_updated.data?.[eval(key)].db_name).to.eq("ut_next_dbname");
        }
      });
    });
    describe("Should update multiple muscle group", () => {
      it("When new db_name argument is updated", async () => {
        const dummy_muscle_group = await _createDummyMuscleGroup(2);

        const ut_muscle_group_to_update: { id: number; db_name: string }[] = [];
        for (const key in dummy_muscle_group.data) {
          const tmp = { ...dummy_muscle_group.data[key].dataValues };
          tmp.db_name = "ut_next_dbname" + key;
          ut_muscle_group_to_update.push(tmp);
        }

        const ut_muscle_group_updated = await MuscleGroup_db.update(ut_muscle_group_to_update, { transaction: dummy_muscle_group.transaction });
        ut_muscle_group_updated.transaction.rollback();

        for (const key in ut_muscle_group_updated.data) {
          expect(ut_muscle_group_updated.data[eval(key)]).to.be.an.instanceof(MuscleGroup);
          expect(ut_muscle_group_updated.data[eval(key)].db_name).to.eq(`ut_next_dbname${key}`);
        }
      });
    });
    describe("Should trow an error", () => {
      it("When db_name is empty", async () => {
        const dummy_muscle_group = await _createDummyMuscleGroup(2);

        const ut_muscle_group_to_update: { id: number; db_name: string }[] = [];
        for (const key in dummy_muscle_group.data) {
          const tmp = { ...dummy_muscle_group.data[eval(key)].dataValues };
          tmp.db_name = "";
          ut_muscle_group_to_update.push(tmp);
        }

        try {
          await MuscleGroup_db.update(ut_muscle_group_to_update, { transaction: dummy_muscle_group.transaction });
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq(`Invalid argument: db_name`);
        } finally {
          dummy_muscle_group.transaction.rollback();
        }
      });
      it("When db_name already exist", async () => {
        const dummy_muscle_group = await _createDummyMuscleGroup(2);

        const ut_muscle_group_to_update: { id: number; db_name: string }[] = [];
        for (const key in dummy_muscle_group.data) {
          const tmp = { ...dummy_muscle_group.data[key].dataValues };
          tmp.db_name = dummy_muscle_group.data[0].db_name;
          ut_muscle_group_to_update.push(tmp);
        }

        try {
          await MuscleGroup_db.update(ut_muscle_group_to_update, { transaction: dummy_muscle_group.transaction });
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq(`Invalid argument: db_name "${dummy_muscle_group.data[0].db_name}" already exists.`);
        } finally {
          dummy_muscle_group.transaction.rollback();
        }
      });

      it("When the transaction rollback", async () => {
        const dummy_muscle_group = await _createDummyMuscleGroup(2);

        const ut_muscle_group_to_update: { id: number; db_name: string }[] = [];
        for (const key in dummy_muscle_group.data) {
          const tmp = { ...dummy_muscle_group.data[key].dataValues };
          tmp.db_name = "ut_dbname";
          ut_muscle_group_to_update.push(tmp);
        }

        const ut_muscle_group_updated = await MuscleGroup_db.update(ut_muscle_group_to_update, { transaction: dummy_muscle_group.transaction });

        dummy_muscle_group.transaction.rollback();

        expect(ut_muscle_group_updated).to.be.an("object");
        expect(ut_muscle_group_updated.data).to.be.null;
        expect(ut_muscle_group_updated.transaction).to.be.an.instanceof(Transaction);
      });
    });
  });

  describe("MuscleGroup Delete Suite", () => {
    describe("Should delete a muscle group ", () => {
      it("When a MuscleGroup object arguments is provided", async () => {
        const dummy_muscle_group = await _createDummyMuscleGroup(1);
        const ut_muscle_group_deleted = await MuscleGroup_db.deleteByObject(dummy_muscle_group.data, { force: true, transaction: dummy_muscle_group.transaction });
        dummy_muscle_group.transaction.rollback();
        expect(ut_muscle_group_deleted.data).to.eq(1);
      });
      it("When an id arguments is provided", async () => {
        const dummy_muscle_group = await _createDummyMuscleGroup(1);
        const ut_muscle_group_deleted = await MuscleGroup_db.deleteByIds([dummy_muscle_group.data[0].id], { force: true, transaction: dummy_muscle_group.transaction });
        dummy_muscle_group.transaction.rollback();
        expect(ut_muscle_group_deleted.data).to.eq(1);
      });
    });
    describe("Should delete multiple muscle group ", () => {
      it("When multiple MuscleGroup objects arguments are provided", async () => {
        const dummy_muscle_group = await _createDummyMuscleGroup(5);
        const ut_muscle_group_deleted = await MuscleGroup_db.deleteByObject(dummy_muscle_group.data, { force: true, transaction: dummy_muscle_group.transaction });
        dummy_muscle_group.transaction.rollback();
        expect(ut_muscle_group_deleted.data).to.eq(5);
      });
      it("When multiple ids arguments are provided", async () => {
        const dummy_muscle_group = await _createDummyMuscleGroup(5);
        const id_list: number[] = [];
        for (const mg of dummy_muscle_group.data) {
          const { id } = mg;
          id_list.push(id);
        }
        const ut_muscle_group_deleted = await MuscleGroup_db.deleteByIds(id_list, { force: true, transaction: dummy_muscle_group.transaction });
        dummy_muscle_group.transaction.rollback();
        expect(ut_muscle_group_deleted.data).to.eq(5);
      });
    });
    describe("Should trow an error", () => {
      it("When no MuscleGroup object was provided", async () => {
        try {
          await MuscleGroup_db.deleteByObject([]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("No MuscleGroup object was provided as an argument on deletion.");
        }
      });
      it("When MuscleGroup object provided has no id", async () => {
        try {
          await MuscleGroup_db.deleteByObject([{ db_name: "ut_dbname" }]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("Invalid argument: id");
        }
      });
      it("When MuscleGroup object provided does not exist", async () => {
        try {
          await MuscleGroup_db.deleteByObject([{ id: -1, db_name: "ut_dbname" }]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("Error when deleting one or more MuscleGroup object(s).");
        }
      });
      it("When no ids was provided", async () => {
        try {
          await MuscleGroup_db.deleteByIds([]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("No ids was provided as an argument on deletion.");
        }
      });
      it("When ids provided does not exist", async () => {
        try {
          await MuscleGroup_db.deleteByIds([-1]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq(`Error when deleting one or more MuscleGroup object(s) with id list.`);
        }
      });
    });
  });

  describe("MuscleGroup Finding Suite", () => {
    describe("Should return muscle_group", () => {
      it("When trying to get all without filter", async () => {
        const dummy_muscle_group = await _createDummyMuscleGroup(5);
        const dummy_length = dummy_muscle_group.data.length;
        const ut_muscle_group_founded = await MuscleGroup_db.getAll(dummy_muscle_group.transaction);
        dummy_muscle_group.transaction.rollback();

        expect(ut_muscle_group_founded).to.be.an("array");
        // expect(ut_muscle_group_founded).to.have.lengthOf(dummy_length - 1);
        for (const muscle_group of dummy_muscle_group.data) {
          expect(muscle_group).to.be.instanceOf(MuscleGroup);
        }
      });
      it("When trying to get all with filter isDelete", async () => {
        const dummy_muscle_group = await _createDummyMuscleGroup(5);
        const ut_muscle_group_founded = await MuscleGroup_db.getAll(dummy_muscle_group.transaction, { isDelete: true });
        dummy_muscle_group.transaction.rollback();

        expect(ut_muscle_group_founded).to.be.an("array");
        for (const muscle_group of dummy_muscle_group.data) {
          expect(muscle_group).to.be.instanceOf(MuscleGroup);
        }
      });
      it("When trying to get all with filter includeDeleted", async () => {
        const dummy_muscle_group = await _createDummyMuscleGroup(5);
        const ut_muscle_group_founded = await MuscleGroup_db.getAll(dummy_muscle_group.transaction, { includeDeleted: true });
        dummy_muscle_group.transaction.rollback();

        expect(ut_muscle_group_founded).to.be.an("array");
        for (const muscle_group of dummy_muscle_group.data) {
          expect(muscle_group).to.be.instanceOf(MuscleGroup);
        }
      });
      it("When his id was provided without filter", async () => {
        const dummy_muscle_group = await _createDummyMuscleGroup(1);
        const ut_muscle_group_founded = await MuscleGroup_db.getByIds([dummy_muscle_group.data[0].id], { transaction: dummy_muscle_group.transaction });
        dummy_muscle_group.transaction.rollback();

        expect(ut_muscle_group_founded[0]).to.be.an.instanceof(MuscleGroup);
        expect(ut_muscle_group_founded[0].id).to.eq(dummy_muscle_group.data[0].id);
      });
      it("When his id was provided with filter isDelete", async () => {
        const dummy_muscle_group = await _createDummyMuscleGroup(5);
        const ut_muscle_group_founded = await MuscleGroup_db.getByIds([dummy_muscle_group.data[0].id], { transaction: dummy_muscle_group.transaction }, { isDelete: true });
        dummy_muscle_group.transaction.rollback();

        expect(ut_muscle_group_founded).to.be.an("array");
        for (const muscle_group of dummy_muscle_group.data) {
          expect(muscle_group).to.be.instanceOf(MuscleGroup);
        }
      });
      it("When his id was provided with filter includeDeleted", async () => {
        const dummy_muscle_group = await _createDummyMuscleGroup(5);
        const ut_muscle_group_founded = await MuscleGroup_db.getByIds([dummy_muscle_group.data[0].id], { transaction: dummy_muscle_group.transaction }, { includeDeleted: true });
        dummy_muscle_group.transaction.rollback();

        expect(ut_muscle_group_founded).to.be.an("array");
        for (const muscle_group of dummy_muscle_group.data) {
          expect(muscle_group).to.be.instanceOf(MuscleGroup);
        }
      });
      it("When his db_name was provided", async () => {
        const dummy_muscle_group = await _createDummyMuscleGroup(1);
        const ut_muscle_group_founded = await MuscleGroup_db.getByDbNames([dummy_muscle_group.data[0].db_name], { transaction: dummy_muscle_group.transaction });
        dummy_muscle_group.transaction.rollback();

        expect(ut_muscle_group_founded[0]).to.be.an.instanceof(MuscleGroup);
        expect(ut_muscle_group_founded[0].db_name).to.eq(dummy_muscle_group.data[0].db_name);
      });
      it("When his db_name was provided with filter isDelete", async () => {
        const dummy_muscle_group = await _createDummyMuscleGroup(5);
        const ut_muscle_group_founded = await MuscleGroup_db.getByDbNames([dummy_muscle_group.data[0].db_name], { transaction: dummy_muscle_group.transaction }, { isDelete: true });
        dummy_muscle_group.transaction.rollback();

        expect(ut_muscle_group_founded).to.be.an("array");
        for (const muscle_group of dummy_muscle_group.data) {
          expect(muscle_group).to.be.instanceOf(MuscleGroup);
        }
      });
      it("When his db_name was provided with filter includeDeleted", async () => {
        const dummy_muscle_group = await _createDummyMuscleGroup(5);
        const ut_muscle_group_founded = await MuscleGroup_db.getByDbNames([dummy_muscle_group.data[0].db_name], { transaction: dummy_muscle_group.transaction }, { includeDeleted: true });
        dummy_muscle_group.transaction.rollback();

        expect(ut_muscle_group_founded).to.be.an("array");
        for (const muscle_group of dummy_muscle_group.data) {
          expect(muscle_group).to.be.instanceOf(MuscleGroup);
        }
      });
      it("When multiple ids was provided", async () => {
        const dummy_muscle_group = await _createDummyMuscleGroup(2);
        const ut_muscle_group_founded = await MuscleGroup_db.getByIds([dummy_muscle_group.data[0].id, dummy_muscle_group.data[1].id], { transaction: dummy_muscle_group.transaction });
        dummy_muscle_group.transaction.rollback();

        expect(ut_muscle_group_founded).to.be.an("array");
        for (const key in dummy_muscle_group.data) {
          expect(ut_muscle_group_founded[eval(key)]).to.be.an.instanceof(MuscleGroup);
          expect(ut_muscle_group_founded[eval(key)].id).to.eq(dummy_muscle_group.data[eval(key)].id);
        }
      });
      it("When multiple db_name was provided", async () => {
        const dummy_muscle_group = await _createDummyMuscleGroup(2);
        const ut_muscle_group_founded = await MuscleGroup_db.getByDbNames([dummy_muscle_group.data[0].db_name, dummy_muscle_group.data[1].db_name], { transaction: dummy_muscle_group.transaction });
        dummy_muscle_group.transaction.rollback();

        expect(ut_muscle_group_founded).to.be.an("array");
        for (const key in dummy_muscle_group.data) {
          expect(ut_muscle_group_founded[eval(key)]).to.be.an.instanceof(MuscleGroup);
          expect(ut_muscle_group_founded[eval(key)].db_name).to.eq(dummy_muscle_group.data[eval(key)].db_name);
        }
      });
      describe("Should throw an error", () => {
        it("When no ids was provided", async () => {
          try {
            await MuscleGroup_db.getByIds([]);
          } catch (e: any) {
            expect(e).to.be.an("Error");
            expect(e.message).to.eq("No id was provided as an argument on finding.");
          }
        });
        it("When no db_name was provided", async () => {
          try {
            await MuscleGroup_db.getByDbNames([]);
          } catch (e: any) {
            expect(e).to.be.an("Error");
            expect(e.message).to.eq("No db_name was provided as an argument on finding.");
          }
        });
      });
    });
  });
});

// /** Create a Dummy MuscleGroup */
const _createDummyMuscleGroup = async (number: number): Promise<{ data: MuscleGroup[]; transaction: Transaction }> => {
  const dummy_list: { db_name: string }[] = [];
  for (let i = 0; i < number; i++) {
    dummy_list.push({ db_name: `dummy_dbname_${i}` });
  }

  let transaction = await sequelize.transaction();
  const dummy_muscule_group_upderted = await MuscleGroup.bulkCreate(dummy_list, { transaction });

  return { data: dummy_muscule_group_upderted, transaction };
};

// /** Destroy the Dummy muscle_group */
// const _destroyDummyMuscleGroup = async (dummy_muscle_group: MuscleGroup | MuscleGroup[], options?: { force?: boolean; transaction?: Transaction }) => {
//   const force = options?.force ? options.force : false;
//   const transaction = options?.transaction ? options.transaction : await sequelize.transaction();

//   if (Array.isArray(dummy_muscle_group)) {
//     const id_list: number[] = [];
//     for (const mg of dummy_muscle_group) {
//       const { id } = mg;
//       id_list.push(id);
//     }
//     return await MuscleGroup.destroy({ where: { id: id_list }, force, transaction });
//   } else return await dummy_muscle_group.destroy({ force, transaction });
// };
