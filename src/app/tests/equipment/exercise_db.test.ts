import { expect } from "chai";
import { Transaction } from "sequelize";
import { sequelize } from "../../models";
import * as Exercise_db from "../../services/exercise/exercise.db";
import Exercise, { ExerciseInput, ExerciseOutput } from "../../models/Exercise.model";

describe("Exercise DB Test Suite", () => {
  describe("Exercise Create Suite", () => {
    describe("Should create a new exercise", () => {
      it("When all minimum arguments are provided", async () => {
        const db_name = "ut_dbname";
        const ut_exercise_created = await Exercise_db.create([{ db_name }]);
        await ut_exercise_created.transaction.rollback();

        expect(ut_exercise_created.data).to.be.an("array");
        expect(ut_exercise_created.transaction).to.be.an.instanceof(Transaction);
        for (const key in ut_exercise_created.data) {
          expect(ut_exercise_created.data?.[eval(key)]).to.be.an.instanceof(Exercise);
          expect(ut_exercise_created.data?.[eval(key)].db_name).to.eq(db_name);
        }
      });
    });
    describe("Should create multiple new exercise", () => {
      it("When all minimum arguments are provided", async () => {
        const db_name1 = "ut_dbname1";
        const db_name2 = "ut_dbname2";
        const ut_exercise_created = await Exercise_db.create([{ db_name: db_name1 }, { db_name: db_name2 }]);
        await ut_exercise_created.transaction.rollback();

        expect(ut_exercise_created.data).to.be.an("array");
        expect(ut_exercise_created.transaction).to.be.an.instanceof(Transaction);
        for (const key in ut_exercise_created.data) {
          expect(ut_exercise_created.data?.[eval(key)]).to.be.an.instanceof(Exercise);
          expect(ut_exercise_created.data?.[eval(key)].db_name).to.eq(eval(`db_name${parseInt(key) + 1}`));
        }
      });
    });
    describe("Should trow an error", () => {
      it("When no db_name was provided", async () => {
        try {
          await Exercise_db.create([{}]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("Invalid argument: db_name");
        }
      });
      it("When db_name is empty", async () => {
        try {
          const db_name = "";
          await Exercise_db.create([{ db_name }]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("Invalid argument: db_name");
        }
      });
      it("When db_name already exist", async () => {
        const dummy_exercise = await _createDummyExercise(1);
        try {
          await Exercise_db.create([{ db_name: dummy_exercise.data[0].db_name }], { transaction: dummy_exercise.transaction });
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq(`Invalid argument: db_name "${dummy_exercise.data[0].db_name}" already exists.`);
        } finally {
          dummy_exercise.transaction.rollback();
        }
      });
      it("When the same db_name is inserted several times", async () => {
        try {
          const ut_muscul_group_created = await Exercise_db.create([{ db_name: "ut_dbname" }, { db_name: "ut_dbname" }]);
          ut_muscul_group_created.transaction.rollback();
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq(`Invalid argument: db_name`);
        }
      });
      it("When the transaction rollback", async () => {
        const ut_exercise = await Exercise_db.create([
          { id: 1, db_name: "ut_dbname1" },
          { id: 1, db_name: "ut_dbname2" },
        ]);
        ut_exercise.transaction.rollback();

        expect(ut_exercise).to.be.an("object");
        expect(ut_exercise.data).to.be.null;
        expect(ut_exercise.transaction).to.be.an.instanceof(Transaction);
      });
    });
  });

  describe("Exercise Upsert Suite", () => {
    describe("Should upsert a new exercise", () => {
      it("When all minimum arguments are provided", async () => {
        const db_name = "ut_dbname";
        const ut_exercise_upserted = await Exercise_db.upsert([{ db_name }]);
        await ut_exercise_upserted.transaction.rollback();

        expect(ut_exercise_upserted.data).to.be.an("array");
        expect(ut_exercise_upserted.transaction).to.be.an.instanceof(Transaction);
        for (const key in ut_exercise_upserted.data) {
          expect(ut_exercise_upserted.data?.[eval(key)]).to.be.an.instanceof(Exercise);
          expect(ut_exercise_upserted.data?.[eval(key)].db_name).to.eq(db_name);
        }
      });
    });
    describe("Should upsert multiple new exercise", () => {
      it("When all minimum arguments are provided", async () => {
        const db_name1 = "ut_dbname1";
        const db_name2 = "ut_dbname2";
        const ut_exercise_upserted = await Exercise_db.upsert([{ db_name: db_name1 }, { db_name: db_name2 }]);
        await ut_exercise_upserted.transaction.rollback();

        expect(ut_exercise_upserted.data).to.be.an("array");
        expect(ut_exercise_upserted.transaction).to.be.an.instanceof(Transaction);
        for (const key in ut_exercise_upserted.data) {
          expect(ut_exercise_upserted.data?.[eval(key)]).to.be.an.instanceof(Exercise);
          expect(ut_exercise_upserted.data?.[eval(key)].db_name).to.eq(eval(`db_name${parseInt(key) + 1}`));
        }
      });
    });
    describe("Should trow an error", () => {
      it("When no db_name was provided", async () => {
        try {
          await Exercise_db.upsert([{}]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("Invalid argument: db_name");
        }
      });
      it("When db_name is empty", async () => {
        try {
          const db_name = "";
          await Exercise_db.upsert([{ db_name }]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("Invalid argument: db_name");
        }
      });
      it("When db_name already exist", async () => {
        const dummy_exercise = await _createDummyExercise(1);
        try {
          await Exercise_db.upsert([{ db_name: dummy_exercise.data[0].db_name }], { transaction: dummy_exercise.transaction });
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq(`Invalid argument: db_name "${dummy_exercise.data[0].db_name}" already exists.`);
        } finally {
          dummy_exercise.transaction.rollback();
        }
      });
      it("When the same db_name is inserted several times", async () => {
        try {
          const ut_muscul_group_upserted = await Exercise_db.upsert([{ db_name: "ut_dbname" }, { db_name: "ut_dbname" }]);
          ut_muscul_group_upserted.transaction.rollback();
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq(`Invalid argument: db_name`);
        }
      });
      // Can't find a solution for generate an rollback test.
      // it("When the transaction rollback", async () => {
      //   const ut_exercise_upserted = await Exercise_db.upsert([{ id: -1, db_name: "" }]);
      //   ut_exercise_upserted.transaction.rollback();

      //   expect(ut_exercise_upserted).to.be.an("object");
      //   expect(ut_exercise_upserted.data).to.be.null;
      //   expect(ut_exercise_upserted.transaction).to.be.an.instanceof(Transaction);
      // });
    });
  });

  describe("Exercise Update Suite", () => {
    describe("Should update a exercise", () => {
      it("When new db_name argument is updated", async () => {
        const dummy_exercise = await _createDummyExercise(1);
        dummy_exercise.data[0].db_name = "ut_next_dbname";
        const ut_exercise_updated = await Exercise_db.update([dummy_exercise.data[0].dataValues], { transaction: dummy_exercise.transaction });
        ut_exercise_updated.transaction.rollback();

        expect(ut_exercise_updated.data).to.be.an("array");
        expect(ut_exercise_updated.transaction).to.be.an.instanceof(Transaction);
        for (const key in ut_exercise_updated.data) {
          expect(ut_exercise_updated.data?.[eval(key)]).to.be.an.instanceof(Exercise);
          expect(ut_exercise_updated.data?.[eval(key)].db_name).to.eq("ut_next_dbname");
        }
      });
    });
    describe("Should update multiple exercise", () => {
      it("When new db_name argument is updated", async () => {
        const dummy_exercise = await _createDummyExercise(2);

        const ut_exercise_to_update: { id: number; db_name: string }[] = [];
        for (const key in dummy_exercise.data) {
          const tmp = { ...dummy_exercise.data[key].dataValues };
          tmp.db_name = "ut_next_dbname" + key;
          ut_exercise_to_update.push(tmp);
        }

        const ut_exercise_updated = await Exercise_db.update(ut_exercise_to_update, { transaction: dummy_exercise.transaction });
        ut_exercise_updated.transaction.rollback();

        for (const key in ut_exercise_updated.data) {
          expect(ut_exercise_updated.data[eval(key)]).to.be.an.instanceof(Exercise);
          expect(ut_exercise_updated.data[eval(key)].db_name).to.eq(`ut_next_dbname${key}`);
        }
      });
    });
    describe("Should trow an error", () => {
      it("When db_name is empty", async () => {
        const dummy_exercise = await _createDummyExercise(2);

        const ut_exercise_to_update: { id: number; db_name: string }[] = [];
        for (const key in dummy_exercise.data) {
          const tmp = { ...dummy_exercise.data[eval(key)].dataValues };
          tmp.db_name = "";
          ut_exercise_to_update.push(tmp);
        }

        try {
          await Exercise_db.update(ut_exercise_to_update, { transaction: dummy_exercise.transaction });
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq(`Invalid argument: db_name`);
        } finally {
          dummy_exercise.transaction.rollback();
        }
      });
      it("When db_name already exist", async () => {
        const dummy_exercise = await _createDummyExercise(2);

        const ut_exercise_to_update: { id: number; db_name: string }[] = [];
        for (const key in dummy_exercise.data) {
          const tmp = { ...dummy_exercise.data[key].dataValues };
          tmp.db_name = dummy_exercise.data[0].db_name;
          ut_exercise_to_update.push(tmp);
        }

        try {
          await Exercise_db.update(ut_exercise_to_update, { transaction: dummy_exercise.transaction });
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq(`Invalid argument: db_name "${dummy_exercise.data[0].db_name}" already exists.`);
        } finally {
          dummy_exercise.transaction.rollback();
        }
      });

      it("When the transaction rollback", async () => {
        const dummy_exercise = await _createDummyExercise(2);

        const ut_exercise_to_update: { id: number; db_name: string }[] = [];
        for (const key in dummy_exercise.data) {
          const tmp = { ...dummy_exercise.data[key].dataValues };
          tmp.db_name = "ut_dbname";
          ut_exercise_to_update.push(tmp);
        }

        const ut_exercise_updated = await Exercise_db.update(ut_exercise_to_update, { transaction: dummy_exercise.transaction });

        dummy_exercise.transaction.rollback();

        expect(ut_exercise_updated).to.be.an("object");
        expect(ut_exercise_updated.data).to.be.null;
        expect(ut_exercise_updated.transaction).to.be.an.instanceof(Transaction);
      });
    });
  });

  describe("Exercise Delete Suite", () => {
    describe("Should delete a exercise ", () => {
      it("When a Exercise object arguments is provided", async () => {
        const dummy_exercise = await _createDummyExercise(1);
        const ut_exercise_deleted = await Exercise_db.deleteByObject(dummy_exercise.data, { force: true, transaction: dummy_exercise.transaction });
        dummy_exercise.transaction.rollback();
        expect(ut_exercise_deleted.data).to.eq(1);
      });
      it("When an id arguments is provided", async () => {
        const dummy_exercise = await _createDummyExercise(1);
        const ut_exercise_deleted = await Exercise_db.deleteByIds([dummy_exercise.data[0].id], { force: true, transaction: dummy_exercise.transaction });
        dummy_exercise.transaction.rollback();
        expect(ut_exercise_deleted.data).to.eq(1);
      });
    });
    describe("Should delete multiple exercise ", () => {
      it("When multiple Exercise objects arguments are provided", async () => {
        const dummy_exercise = await _createDummyExercise(5);
        const ut_exercise_deleted = await Exercise_db.deleteByObject(dummy_exercise.data, { force: true, transaction: dummy_exercise.transaction });
        dummy_exercise.transaction.rollback();
        expect(ut_exercise_deleted.data).to.eq(5);
      });
      it("When multiple ids arguments are provided", async () => {
        const dummy_exercise = await _createDummyExercise(5);
        const id_list: number[] = [];
        for (const mg of dummy_exercise.data) {
          const { id } = mg;
          id_list.push(id);
        }
        const ut_exercise_deleted = await Exercise_db.deleteByIds(id_list, { force: true, transaction: dummy_exercise.transaction });
        dummy_exercise.transaction.rollback();
        expect(ut_exercise_deleted.data).to.eq(5);
      });
    });
    describe("Should trow an error", () => {
      it("When no Exercise object was provided", async () => {
        try {
          await Exercise_db.deleteByObject([]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("No Exercise object was provided as an argument on deletion.");
        }
      });
      it("When Exercise object provided has no id", async () => {
        try {
          await Exercise_db.deleteByObject([{ db_name: "ut_dbname" }]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("Invalid argument: id");
        }
      });
      it("When Exercise object provided does not exist", async () => {
        try {
          await Exercise_db.deleteByObject([{ id: -1, db_name: "ut_dbname" }]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("Error when deleting one or more Exercise object(s).");
        }
      });
      it("When no ids was provided", async () => {
        try {
          await Exercise_db.deleteByIds([]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq("No ids was provided as an argument on deletion.");
        }
      });
      it("When ids provided does not exist", async () => {
        try {
          await Exercise_db.deleteByIds([-1]);
        } catch (e: any) {
          expect(e).to.be.an("Error");
          expect(e.message).to.eq(`Error when deleting one or more Exercise object(s) with id list.`);
        }
      });
    });
  });

  describe("Exercise Finding Suite", () => {
    describe("Should return exercise", () => {
      it("When trying to get all without filter", async () => {
        const dummy_exercise = await _createDummyExercise(5);
        const dummy_length = dummy_exercise.data.length;
        const ut_exercise_founded = await Exercise_db.getAll(dummy_exercise.transaction);
        dummy_exercise.transaction.rollback();

        expect(ut_exercise_founded).to.be.an("array");
        // expect(ut_exercise_founded).to.have.lengthOf(dummy_length - 1);
        for (const exercise of dummy_exercise.data) {
          expect(exercise).to.be.instanceOf(Exercise);
        }
      });
      it("When trying to get all with filter isDelete", async () => {
        const dummy_exercise = await _createDummyExercise(5);
        const ut_exercise_founded = await Exercise_db.getAll(dummy_exercise.transaction, { isDelete: true });
        dummy_exercise.transaction.rollback();

        expect(ut_exercise_founded).to.be.an("array");
        for (const exercise of dummy_exercise.data) {
          expect(exercise).to.be.instanceOf(Exercise);
        }
      });
      it("When trying to get all with filter includeDeleted", async () => {
        const dummy_exercise = await _createDummyExercise(5);
        const ut_exercise_founded = await Exercise_db.getAll(dummy_exercise.transaction, { includeDeleted: true });
        dummy_exercise.transaction.rollback();

        expect(ut_exercise_founded).to.be.an("array");
        for (const exercise of dummy_exercise.data) {
          expect(exercise).to.be.instanceOf(Exercise);
        }
      });
      it("When his id was provided without filter", async () => {
        const dummy_exercise = await _createDummyExercise(1);
        const ut_exercise_founded = await Exercise_db.getByIds([dummy_exercise.data[0].id], { transaction: dummy_exercise.transaction });
        dummy_exercise.transaction.rollback();

        expect(ut_exercise_founded[0]).to.be.an.instanceof(Exercise);
        expect(ut_exercise_founded[0].id).to.eq(dummy_exercise.data[0].id);
      });
      it("When his id was provided with filter isDelete", async () => {
        const dummy_exercise = await _createDummyExercise(5);
        const ut_exercise_founded = await Exercise_db.getByIds([dummy_exercise.data[0].id], { transaction: dummy_exercise.transaction }, { isDelete: true });
        dummy_exercise.transaction.rollback();

        expect(ut_exercise_founded).to.be.an("array");
        for (const exercise of dummy_exercise.data) {
          expect(exercise).to.be.instanceOf(Exercise);
        }
      });
      it("When his id was provided with filter includeDeleted", async () => {
        const dummy_exercise = await _createDummyExercise(5);
        const ut_exercise_founded = await Exercise_db.getByIds([dummy_exercise.data[0].id], { transaction: dummy_exercise.transaction }, { includeDeleted: true });
        dummy_exercise.transaction.rollback();

        expect(ut_exercise_founded).to.be.an("array");
        for (const exercise of dummy_exercise.data) {
          expect(exercise).to.be.instanceOf(Exercise);
        }
      });
      it("When his db_name was provided", async () => {
        const dummy_exercise = await _createDummyExercise(1);
        const ut_exercise_founded = await Exercise_db.getByDbNames([dummy_exercise.data[0].db_name], { transaction: dummy_exercise.transaction });
        dummy_exercise.transaction.rollback();

        expect(ut_exercise_founded[0]).to.be.an.instanceof(Exercise);
        expect(ut_exercise_founded[0].db_name).to.eq(dummy_exercise.data[0].db_name);
      });
      it("When his db_name was provided with filter isDelete", async () => {
        const dummy_exercise = await _createDummyExercise(5);
        const ut_exercise_founded = await Exercise_db.getByDbNames([dummy_exercise.data[0].db_name], { transaction: dummy_exercise.transaction }, { isDelete: true });
        dummy_exercise.transaction.rollback();

        expect(ut_exercise_founded).to.be.an("array");
        for (const exercise of dummy_exercise.data) {
          expect(exercise).to.be.instanceOf(Exercise);
        }
      });
      it("When his db_name was provided with filter includeDeleted", async () => {
        const dummy_exercise = await _createDummyExercise(5);
        const ut_exercise_founded = await Exercise_db.getByDbNames([dummy_exercise.data[0].db_name], { transaction: dummy_exercise.transaction }, { includeDeleted: true });
        dummy_exercise.transaction.rollback();

        expect(ut_exercise_founded).to.be.an("array");
        for (const exercise of dummy_exercise.data) {
          expect(exercise).to.be.instanceOf(Exercise);
        }
      });
      it("When multiple ids was provided", async () => {
        const dummy_exercise = await _createDummyExercise(2);
        const ut_exercise_founded = await Exercise_db.getByIds([dummy_exercise.data[0].id, dummy_exercise.data[1].id], { transaction: dummy_exercise.transaction });
        dummy_exercise.transaction.rollback();

        expect(ut_exercise_founded).to.be.an("array");
        for (const key in dummy_exercise.data) {
          expect(ut_exercise_founded[eval(key)]).to.be.an.instanceof(Exercise);
          expect(ut_exercise_founded[eval(key)].id).to.eq(dummy_exercise.data[eval(key)].id);
        }
      });
      it("When multiple db_name was provided", async () => {
        const dummy_exercise = await _createDummyExercise(2);
        const ut_exercise_founded = await Exercise_db.getByDbNames([dummy_exercise.data[0].db_name, dummy_exercise.data[1].db_name], { transaction: dummy_exercise.transaction });
        dummy_exercise.transaction.rollback();

        expect(ut_exercise_founded).to.be.an("array");
        for (const key in dummy_exercise.data) {
          expect(ut_exercise_founded[eval(key)]).to.be.an.instanceof(Exercise);
          expect(ut_exercise_founded[eval(key)].db_name).to.eq(dummy_exercise.data[eval(key)].db_name);
        }
      });
      describe("Should throw an error", () => {
        it("When no ids was provided", async () => {
          try {
            await Exercise_db.getByIds([]);
          } catch (e: any) {
            expect(e).to.be.an("Error");
            expect(e.message).to.eq("No id was provided as an argument on finding.");
          }
        });
        it("When no db_name was provided", async () => {
          try {
            await Exercise_db.getByDbNames([]);
          } catch (e: any) {
            expect(e).to.be.an("Error");
            expect(e.message).to.eq("No db_name was provided as an argument on finding.");
          }
        });
      });
    });
  });
});

// /** Create a Dummy Exercise */
const _createDummyExercise = async (number: number): Promise<{ data: Exercise[]; transaction: Transaction }> => {
  const dummy_list: { db_name: string }[] = [];
  for (let i = 0; i < number; i++) {
    dummy_list.push({ db_name: `dummy_dbname_${i}` });
  }

  let transaction = await sequelize.transaction();
  const dummy_muscule_group_upderted = await Exercise.bulkCreate(dummy_list, { transaction });

  return { data: dummy_muscule_group_upderted, transaction };
};

// /** Destroy the Dummy exercise */
// const _destroyDummyExercise = async (dummy_exercise: Exercise | Exercise[], options?: { force?: boolean; transaction?: Transaction }) => {
//   const force = options?.force ? options.force : false;
//   const transaction = options?.transaction ? options.transaction : await sequelize.transaction();

//   if (Array.isArray(dummy_exercise)) {
//     const id_list: number[] = [];
//     for (const mg of dummy_exercise) {
//       const { id } = mg;
//       id_list.push(id);
//     }
//     return await Exercise.destroy({ where: { id: id_list }, force, transaction });
//   } else return await dummy_exercise.destroy({ force, transaction });
// };
