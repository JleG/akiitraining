const moment = require("moment");
const validate = require("validate.js");

validate.formatters.custom = function (errors) {
  let errorList = [];
  errors.map(function (error) {
    errorList.push({
      // attribute: error.attribute,
      // value: error.value,
      validator: error.validator,
      attributes: error.attributes,
      code: error.error,
    });
  });
  return errorList;
};

validate.extend(validate.validators.datetime, {
  parse: function (value, options) {
    return +moment.utc(value);
  },
  format: function (value, options) {
    var format = options.dateOnly ? "YYYY-MM-DD" : "YYYY-MM-DD hh:mm:ss";
    return moment.utc(value).format(format);
  },
});
