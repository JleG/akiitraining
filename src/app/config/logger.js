const { format, createLogger, transports, addColors } = require("winston");
const { timestamp, combine, printf, errors, json } = format;
require("winston-daily-rotate-file");

const consoleFormat = printf(({ level, message, timestamp, stack, ...metadata }) => {
  return `${timestamp} [${level}]: ${stack || message}`;
});

const logsFormat = format.printf(({ level, message, timestamp, ...metadata }) => {
  let msg = `${timestamp} [${level}] : ${message} `;
  if (metadata) {
    msg += JSON.stringify(metadata);
  }
  return msg;
});

const infoAndWarnFilter = format((info, opts) => {
  return info.level === "info" || info.level === "warn" ? info : false;
});
const errorFilter = format((info, opts) => {
  return info.level === "error" ? info : false;
});
const debugFilter = format((info, opts) => {
  return info.level === "debug" ? info : false;
});

const customLevels = {
  levels: {
    error: 0,
    warn: 1,
    info: 2,
    debug: 3,
    all: 4,
  },
  colors: {
    error: "red",
    warn: "yellow",
    info: "blue",
    debug: "grey",
    all: "white",
  },
};

const logger = createLogger({
  levels: customLevels.levels,
  format: combine(timestamp({ format: "DD-MM-YYYY HH:mm:ss" }), errors({ stack: true }), json(), consoleFormat),
  exitOnError: false,
  transports: [
    new transports.Console({
      format: combine(format.colorize(), consoleFormat),
      level: "all",
    }),
    new transports.DailyRotateFile({
      filename: "./logs/%DATE% - errors.log",
      datePattern: "DD-MM-YYYY",
      zippedArchive: true,
      level: "error",
      format: combine(errorFilter(), logsFormat),
    }),
    new transports.DailyRotateFile({
      filename: "./logs/%DATE% - warn-info.log",
      datePattern: "DD-MM-YYYY",
      zippedArchive: true,
      level: "info",
      format: combine(infoAndWarnFilter(), logsFormat),
    }),
    new transports.DailyRotateFile({
      filename: "./logs/%DATE% - debug.log",
      datePattern: "DD-MM-YYYY",
      zippedArchive: true,
      level: "debug",
      format: combine(debugFilter(), logsFormat),
    }),
  ],
});

const requestLogger = createLogger({
  levels: customLevels.levels,
  format: combine(timestamp({ format: "DD-MM-YYYY HH:mm:ss" }), errors({ stack: true }), json(), consoleFormat),
  exitOnError: false,
  transports: [
    new transports.Console({
      format: combine(format.colorize(), consoleFormat),
      level: "all",
    }),
    new transports.DailyRotateFile({
      filename: "./logs/%DATE% - errors.log",
      datePattern: "DD-MM-YYYY",
      zippedArchive: true,
      level: "error",
      format: combine(errorFilter(), logsFormat),
    }),
    new transports.DailyRotateFile({
      filename: "./logs/%DATE% - warn-info.log",
      datePattern: "DD-MM-YYYY",
      zippedArchive: true,
      level: "info",
      format: combine(infoAndWarnFilter(), logsFormat),
    }),
    new transports.DailyRotateFile({
      filename: "./logs/%DATE% - debug.log",
      datePattern: "DD-MM-YYYY",
      zippedArchive: true,
      level: "debug",
      format: combine(debugFilter(), logsFormat),
    }),
  ],
});

addColors(customLevels.colors);

module.exports = { logger, requestLogger };
