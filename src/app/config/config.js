require("dotenv").config();

const node_env = process.env.NODE_ENV || "development";
const dialect = process.env.DIALECT;

const host = process.env.MYSQL_HOST;
const port = process.env.MYSQL_PORT;
const user = process.env.MYSQL_USER;
const password = process.env.MYSQL_PASSWORD;
const database = process.env.MYSQL_DATABASE;
const timezone = process.env.MYSQL_TIMEZONE;

const session_secret = process.env.SESSION_SECRET;

const config = {
  development: {
    db: {
      dialect,
      host,
      port,
      user,
      password,
      database,
      timezone,
    },
    session_secret,
  },
};

module.exports = config[node_env];
