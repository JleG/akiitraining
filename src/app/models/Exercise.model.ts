import { DataTypes, Model, Optional } from "sequelize";
import { sequelize } from ".";

interface ExerciseAttributes {
  id: number;
  db_name: string;
  description: string;
}

export interface ExerciseInput extends Optional<ExerciseAttributes, "id" | "description"> {}
export interface ExerciseOutput extends Required<ExerciseAttributes> {}

class Exercise extends Model<ExerciseAttributes, ExerciseInput> implements ExerciseAttributes {
  public id!: number;
  public db_name!: string;
  public description!: string;
}

Exercise.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    db_name: {
      type: DataTypes.STRING(100),
      allowNull: false,
      unique: true,
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
  },
  {
    sequelize,
    underscored: true,
    timestamps: true,
    paranoid: true, // Soft Delete
  }
);

export default Exercise;
