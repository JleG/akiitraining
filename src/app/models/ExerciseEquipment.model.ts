import { DataTypes, Model, Optional } from "sequelize";
import { sequelize } from ".";

// External Models
import Exercise from "./Exercise.model";
import Equipment from "./Equipment.model";
import MuscleGroup from "./MuscleGroup.model";

interface ExerciseEquipmentAttributes {
  id: number;
  exerciseId: number;
  equipmentId: number;
}

export interface ExerciseEquipmentInput extends Optional<ExerciseEquipmentAttributes, "id"> {}
export interface ExerciseEquipmentOutput extends Required<ExerciseEquipmentAttributes> {}

class ExerciseEquipment extends Model<ExerciseEquipmentAttributes, ExerciseEquipmentInput> implements ExerciseEquipmentAttributes {
  public id!: number;
  public exerciseId!: number;
  public equipmentId!: number;
}

ExerciseEquipment.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    exerciseId: {
      type: DataTypes.INTEGER,
      references: {
        model: Exercise,
        key: "id",
      },
    },
    equipmentId: {
      type: DataTypes.INTEGER,
      references: {
        model: Equipment,
        key: "id",
      },
    },
  },
  {
    sequelize,
    underscored: true,
    timestamps: false,
    // paranoid: true, // Soft Delete
  }
);

Exercise.belongsToMany(Equipment, {
  as: "equipments",
  through: ExerciseEquipment,
  timestamps: false,
});

Equipment.belongsToMany(Exercise, {
  as: "exercises",
  through: ExerciseEquipment,
  timestamps: false,
});

export default ExerciseEquipment;
