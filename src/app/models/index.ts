import { Dialect, Sequelize } from "sequelize";
import fs from "fs";
import path from "path";

const config = require(__dirname + "/../config/config.js");

// <Database_connexion>
const sequelize = new Sequelize(config.db.database, config.db.user, config.db.password, {
  host: config.db.host,
  dialect: config.db.dialect as Dialect,
});
// </Database_connexion>

// const models: { [key: string]: string } = {};

// List of models to syncronize
const basename = path.basename(__filename);
fs.readdirSync(__dirname)
  .filter((file) => {
    return file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".ts";
  })
  .forEach((file) => {
    // const model_name = file.charAt(0).toUpperCase() + file.slice(1, -9);
    // models[model_name] = require(`./${file}`);
    require(`./${file}`);
  });

export { Sequelize, sequelize };
