import { DataTypes, Model, Optional } from "sequelize";
import { sequelize } from ".";

interface MuscleGroupAttributes {
  id: number;
  db_name: string;
}

export interface MuscleGroupInput extends Optional<MuscleGroupAttributes, "id" | "db_name"> {}
export interface MuscleGroupOutput extends Required<MuscleGroupAttributes> {}

class MuscleGroup extends Model<MuscleGroupAttributes, MuscleGroupInput> implements MuscleGroupAttributes {
  public id!: number;
  public db_name!: string;
}

MuscleGroup.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    db_name: {
      type: DataTypes.STRING(20),
      allowNull: false,
      unique: true,
    },
  },
  {
    sequelize,
    underscored: true,
    timestamps: true,
    paranoid: true, // Soft Delete
  }
);

export default MuscleGroup;
