import { DataTypes, Model, Optional } from "sequelize";
import { sequelize } from ".";

// Externals models
import Exercise from "./Exercise.model";

interface EquipmentAttributes {
  id: number;
  db_name: string;
}

export interface EquipmentInput extends Optional<EquipmentAttributes, "id" | "db_name"> {}
export interface EquipmentOutput extends Required<EquipmentAttributes> {}

class Equipment extends Model<EquipmentAttributes, EquipmentInput> implements EquipmentAttributes {
  public id!: number;
  public db_name!: string;
}

Equipment.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    db_name: {
      type: DataTypes.STRING(100),
      allowNull: false,
      unique: true,
    },
  },
  {
    sequelize,
    underscored: true,
    timestamps: true,
    paranoid: true, // Soft Delete
  }
);

// Exercise.belongsToMany(Equipment, {
//   as: "equipments",
//   through: "exercise_equipment",
//   foreignKey: { name: 'equipment_id', allowNull: false },
//   timestamps: false,
// });

// Show.belongsToMany(User, { as: 'bands', through: ShowBand, foreignKey: { name: 'id_show', allowNull: false } });

// Equipment.hasMany(Exercise, { foreignKey: { name: "exercise_id", allowNull: false } });

// User.belongsToMany(Show, { through: ShowBand, foreignKey: { name: "id_band", allowNull: false } });

// Equipment.hasMany(Exercise, {
//   sourceKey: "id",
//   foreignKey: "exercise_id",
//   as: "exercises",
// });

// Equipment.belongsToMany(Exercise, {
//   through: "exercise_equipment",
//   as: "exercises",
//   timestamps: false,
// });

export default Equipment;
