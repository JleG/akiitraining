import { DataTypes, Model, Optional } from "sequelize";
import { sequelize } from ".";

// External Models
import Exercise from "./Exercise.model";
import MuscleGroup from "./MuscleGroup.model";

interface ExerciseMuscleGroupAttributes {
  id: number;
  exerciseId: number;
  muscleGroupId: number;
}

export interface ExerciseMuscleGroupInput extends Optional<ExerciseMuscleGroupAttributes, "id"> {}
export interface ExerciseMuscleGroupOutput extends Required<ExerciseMuscleGroupAttributes> {}

class ExerciseMuscleGroup extends Model<ExerciseMuscleGroupAttributes, ExerciseMuscleGroupInput> implements ExerciseMuscleGroupAttributes {
  public id!: number;
  public exerciseId!: number;
  public muscleGroupId!: number;
}

ExerciseMuscleGroup.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    exerciseId: {
      type: DataTypes.INTEGER,
      references: {
        model: Exercise,
        key: "id",
      },
    },
    muscleGroupId: {
      type: DataTypes.INTEGER,
      references: {
        model: MuscleGroup,
        key: "id",
      },
    },
  },
  {
    sequelize,
    underscored: true,
    timestamps: false,
    // paranoid: true, // Soft Delete
  }
);

Exercise.belongsToMany(MuscleGroup, {
  as: "muscle_groups",
  through: ExerciseMuscleGroup,
  timestamps: false,
});

MuscleGroup.belongsToMany(Exercise, {
  as: "exercises",
  through: ExerciseMuscleGroup,
  timestamps: false,
});

export default ExerciseMuscleGroup;
