import { Transaction } from "sequelize";
import * as MuscleGroup_db from "./muscleGroup.db";
import MuscleGroup, { MuscleGroupInput, MuscleGroupOutput } from "../../models/MuscleGroup.model";
import { GetAllMuscleGroupFilters } from "./muscleGroup.types";

export const validateMuscleGroupExistsFactory = (db_name: string, options?: { transaction?: Transaction }, filters?: GetAllMuscleGroupFilters) => {
  if (!db_name) throw new Error("Invalid number of args, please pass a db_name.");
  return MuscleGroup_db.getByDbNames([db_name], options, filters);
};

export const createFactory = (payload: MuscleGroupInput[], options?: { transaction?: Transaction }): Promise<{ data: MuscleGroup[] | null; transaction: Transaction }> => {
  return MuscleGroup_db.create(payload, options);
};

export const updateFactory = (payload: MuscleGroupInput[], options?: { transaction?: Transaction }): Promise<{ data: MuscleGroup[] | null; transaction: Transaction }> => {
  return MuscleGroup_db.update(payload, options);
};

export const deleteFactory = (payload: MuscleGroupInput[], options?: { force?: boolean; transaction?: Transaction }): Promise<{ data: number | null; transaction: Transaction }> => {
  return MuscleGroup_db.deleteByObject(payload, options);
};

export const deleteByIdsFactory = (payload: number[], options?: { force?: boolean; transaction?: Transaction }): Promise<{ data: number | null; transaction: Transaction }> => {
  return MuscleGroup_db.deleteByIds(payload, options);
};

export const getAllFactory = (transaction?: Transaction, filters?: GetAllMuscleGroupFilters): Promise<MuscleGroup[]> => {
  return MuscleGroup_db.getAll(transaction, filters);
};

export const getByIdsFactory = (payload: number[], options?: { transaction?: Transaction }, filters?: GetAllMuscleGroupFilters): Promise<MuscleGroup[]> => {
  return MuscleGroup_db.getByIds(payload, options, filters);
};
export const getByDbNamesFactory = (payload: string[], options?: { transaction?: Transaction }, filters?: GetAllMuscleGroupFilters): Promise<MuscleGroup[]> => {
  return MuscleGroup_db.getByDbNames(payload, options, filters);
};
