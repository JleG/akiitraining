import { Optional } from "sequelize";

export type CreateMuscleGroupDTO = {
  db_name: string;
};

export type UpdateMuscleGroupDTO = {
  id: number;
  db_name: string;
};

export type FilterMuscleGroupDTO = {
  isDeleted?: boolean;
  includeDeleted?: boolean;
};
