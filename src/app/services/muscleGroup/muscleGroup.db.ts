import { Op, Transaction } from "sequelize";
import { sequelize } from "../../models";
import Exercise from "../../models/Exercise.model";
import MuscleGroup, { MuscleGroupInput, MuscleGroupOutput } from "../../models/MuscleGroup.model";
import { GetAllMuscleGroupFilters } from "./muscleGroup.types";

const include = [
  {
    model: Exercise,
    as: "exercises",
    attributes: ["id", "db_name"],
  },
];

export const create = async (payload: MuscleGroupInput[], options?: { transaction?: Transaction }): Promise<{ data: MuscleGroup[] | null; transaction: Transaction }> => {
  const db_name_list: string[] = [];
  for (const mg of payload) {
    const { db_name } = mg;
    if (!db_name || db_name_list.includes(db_name)) throw new Error("Invalid argument: db_name");
    db_name_list.push(db_name);
  }

  const transaction = options?.transaction ? options.transaction : await sequelize.transaction();

  const muscle_group_founded = await MuscleGroup.findAll({ where: { db_name: db_name_list }, transaction });
  if (muscle_group_founded[0]) throw new Error(`Invalid argument: db_name "${muscle_group_founded[0].db_name}" already exists.`);

  try {
    const muscleGroup_inserted = await MuscleGroup.bulkCreate(payload, { transaction });
    return { data: muscleGroup_inserted, transaction };
  } catch (error) {
    await transaction.rollback();
    return { data: null, transaction };
  }
};

export const upsert = async (payload: MuscleGroupInput[], options?: { transaction?: Transaction }): Promise<{ data: MuscleGroup[] | null; transaction: Transaction }> => {
  const db_name_list: string[] = [];
  for (const mg of payload) {
    const { db_name } = mg;
    if (!db_name || db_name_list.includes(db_name)) throw new Error("Invalid argument: db_name");
    db_name_list.push(db_name);
  }

  const transaction = options?.transaction ? options.transaction : await sequelize.transaction();

  const muscle_group_founded = await MuscleGroup.findAll({ where: { db_name: db_name_list }, transaction });
  if (muscle_group_founded[0]) throw new Error(`Invalid argument: db_name "${muscle_group_founded[0].db_name}" already exists.`);

  try {
    const muscleGroup_upserted = await MuscleGroup.bulkCreate(payload, {
      fields: ["db_name"],
      updateOnDuplicate: ["db_name"],
      transaction,
    });
    return { data: muscleGroup_upserted, transaction };
  } catch (error) {
    await transaction.rollback();
    return { data: null, transaction };
  }
};

export const update = async (payload: MuscleGroupInput[], options?: { transaction?: Transaction }): Promise<{ data: MuscleGroup[] | null; transaction: Transaction }> => {
  const db_name_list: string[] = [];
  for (const mg of payload) {
    const { db_name } = mg;
    if (!db_name) throw new Error("Invalid argument: db_name");
    db_name_list.push(db_name);
  }

  const transaction = options?.transaction ? options.transaction : await sequelize.transaction();

  // const muscle_group_founded = await MuscleGroup.findAll({ where: { db_name: db_name_list }, transaction });
  // if (muscle_group_founded[0]) throw new Error(`Invalid argument: db_name "${muscle_group_founded[0].db_name}" already exists.`);

  try {
    const muscleGroup_updated = await MuscleGroup.bulkCreate(payload, {
      updateOnDuplicate: ["db_name"],
      transaction,
    });
    return { data: muscleGroup_updated, transaction };
  } catch (error) {
    await transaction.rollback();
    return { data: null, transaction };
  }
};

export const deleteByObject = async (payload: MuscleGroupInput[], options?: { force?: boolean; transaction?: Transaction }): Promise<{ data: number | null; transaction: Transaction }> => {
  if (!payload[0]) throw new Error("No MuscleGroup object was provided as an argument on deletion.");
  const id_list: number[] = [];
  for (const mg of payload) {
    const { id } = mg;
    if (!id) throw new Error("Invalid argument: id");
    id_list.push(id);
  }

  const transaction = options?.transaction ? options.transaction : await sequelize.transaction();

  try {
    const muscleGroup_deleted = await MuscleGroup.destroy({ where: { id: id_list }, ...options });
    if (muscleGroup_deleted === 0) throw new Error(`Error when deleting one or more MuscleGroup object(s).`);
    return { data: muscleGroup_deleted, transaction };
  } catch (error) {
    await transaction.rollback();
    return { data: null, transaction };
  }
};

export const deleteByIds = async (payload: number[], options?: { force?: boolean; transaction?: Transaction }): Promise<{ data: number | null; transaction: Transaction }> => {
  if (!payload[0]) throw new Error("No ids was provided as an argument on deletion.");

  const transaction = options?.transaction ? options.transaction : await sequelize.transaction();

  try {
    const muscleGroup_deleted = await MuscleGroup.destroy({ where: { id: payload }, ...options });
    if (muscleGroup_deleted === 0) throw new Error(`Error when deleting one or more MuscleGroup object(s) with id list.`);
    return { data: muscleGroup_deleted, transaction };
  } catch (error) {
    await transaction.rollback();
    return { data: null, transaction };
  }
};

export const getAll = async (transaction?: Transaction, filters?: GetAllMuscleGroupFilters): Promise<MuscleGroup[]> => {
  const muscleGroup_founded = await MuscleGroup.findAll({
    where: {
      ...(filters?.isDelete && { deletedAt: { [Op.not]: null } }),
    },
    ...((filters?.isDelete || filters?.includeDeleted) && { paranoid: true }),
    include,
    transaction,
  });
  return muscleGroup_founded;
};

export const getByIds = async (payload: number[], options?: { transaction?: Transaction }, filters?: GetAllMuscleGroupFilters): Promise<MuscleGroup[]> => {
  if (!payload[0]) throw new Error("No id was provided as an argument on finding.");
  const muscleGroup_founded = await MuscleGroup.findAll({ where: { id: payload, ...(filters?.isDelete && { deletedAt: { [Op.not]: null } }) }, ...((filters?.isDelete || filters?.includeDeleted) && { paranoid: true }), ...options, include });
  return muscleGroup_founded;
};

export const getByDbNames = async (payload: string[], options?: { transaction?: Transaction }, filters?: GetAllMuscleGroupFilters): Promise<MuscleGroup[]> => {
  if (!payload[0]) throw new Error("No db_name was provided as an argument on finding.");
  const muscleGroup_founded = await MuscleGroup.findAll({ where: { db_name: payload, ...(filters?.isDelete && { deletedAt: { [Op.not]: null } }) }, ...((filters?.isDelete || filters?.includeDeleted) && { paranoid: true }), ...options, include });
  return muscleGroup_founded;
};
