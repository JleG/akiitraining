export interface GetAllExerciseFilters {
  isDelete?: boolean;
  includeDeleted?: boolean;
}
