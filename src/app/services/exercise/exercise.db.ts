import { Op, Transaction } from "sequelize";
import { sequelize } from "../../models";
import Exercise, { ExerciseInput, ExerciseOutput } from "../../models/Exercise.model";
import { GetAllExerciseFilters } from "./exercise.types";

// External models
import Equipment from "../../models/Equipment.model";
import MuscleGroup from "../../models/MuscleGroup.model";

const include = [
  {
    model: Equipment,
    as: "equipments",
    attributes: ["id", "db_name"],
  },
  {
    model: MuscleGroup,
    as: "muscle_groups",
    attributes: ["id", "db_name"],
  },
];

export const create = async (payload: ExerciseInput[], options?: { transaction?: Transaction }): Promise<{ data: Exercise[] | null; transaction: Transaction }> => {
  const db_name_list: string[] = [];
  for (const ex of payload) {
    const { db_name } = ex;
    if (!db_name || db_name_list.includes(db_name)) throw new Error("Invalid argument: db_name");
    db_name_list.push(db_name);
  }

  const transaction = options?.transaction ? options.transaction : await sequelize.transaction();

  const exercise_founded = await Exercise.findAll({ where: { db_name: db_name_list }, transaction });
  if (exercise_founded[0]) throw new Error(`Invalid argument: db_name "${exercise_founded[0].db_name}" already exists.`);

  try {
    const exercise_inserted = await Exercise.bulkCreate(payload, { transaction });
    return { data: exercise_inserted, transaction };
  } catch (error) {
    await transaction.rollback();
    return { data: null, transaction };
  }
};

export const upsert = async (payload: ExerciseInput[], options?: { transaction?: Transaction }): Promise<{ data: Exercise[] | null; transaction: Transaction }> => {
  const db_name_list: string[] = [];
  for (const ex of payload) {
    const { db_name } = ex;
    if (!db_name || db_name_list.includes(db_name)) throw new Error("Invalid argument: db_name");
    db_name_list.push(db_name);
  }

  const transaction = options?.transaction ? options.transaction : await sequelize.transaction();

  const exercise_founded = await Exercise.findAll({ where: { db_name: db_name_list }, transaction });
  if (exercise_founded[0]) throw new Error(`Invalid argument: db_name "${exercise_founded[0].db_name}" already exists.`);

  try {
    const exercise_upserted = await Exercise.bulkCreate(payload, {
      fields: ["db_name"],
      updateOnDuplicate: ["db_name"],
      transaction,
    });
    return { data: exercise_upserted, transaction };
  } catch (error) {
    await transaction.rollback();
    return { data: null, transaction };
  }
};

export const update = async (payload: ExerciseInput[], options?: { transaction?: Transaction }): Promise<{ data: Exercise[] | null; transaction: Transaction }> => {
  const db_name_list: string[] = [];
  for (const ex of payload) {
    const { db_name } = ex;
    if (!db_name) throw new Error("Invalid argument: db_name");
    db_name_list.push(db_name);
  }

  const transaction = options?.transaction ? options.transaction : await sequelize.transaction();

  // const exercise_founded = await Exercise.findAll({ where: { db_name: db_name_list }, transaction });
  // if (exercise_founded[0]) throw new Error(`Invalid argument: db_name "${exercise_founded[0].db_name}" already exists.`);

  try {
    const exercise_updated = await Exercise.bulkCreate(payload, {
      updateOnDuplicate: ["db_name"],
      transaction,
    });
    return { data: exercise_updated, transaction };
  } catch (error) {
    await transaction.rollback();
    return { data: null, transaction };
  }
};

export const deleteByObject = async (payload: ExerciseInput[], options?: { force?: boolean; transaction?: Transaction }): Promise<{ data: number | null; transaction: Transaction }> => {
  if (!payload[0]) throw new Error("No Exercise object was provided as an argument on deletion.");
  const id_list: number[] = [];
  for (const ex of payload) {
    const { id } = ex;
    if (!id) throw new Error("Invalid argument: id");
    id_list.push(id);
  }

  const transaction = options?.transaction ? options.transaction : await sequelize.transaction();

  try {
    const exercise_deleted = await Exercise.destroy({ where: { id: id_list }, ...options });
    if (exercise_deleted === 0) throw new Error(`Error when deleting one or more Exercise object(s).`);
    return { data: exercise_deleted, transaction };
  } catch (error) {
    await transaction.rollback();
    return { data: null, transaction };
  }
};

export const deleteByIds = async (payload: number[], options?: { force?: boolean; transaction?: Transaction }): Promise<{ data: number | null; transaction: Transaction }> => {
  if (!payload[0]) throw new Error("No ids was provided as an argument on deletion.");

  const transaction = options?.transaction ? options.transaction : await sequelize.transaction();

  try {
    const exercise_deleted = await Exercise.destroy({ where: { id: payload }, ...options });
    if (exercise_deleted === 0) throw new Error(`Error when deleting one or more Exercise object(s) with id list.`);
    return { data: exercise_deleted, transaction };
  } catch (error) {
    await transaction.rollback();
    return { data: null, transaction };
  }
};

export const getAll = async (transaction?: Transaction, filters?: GetAllExerciseFilters): Promise<Exercise[]> => {
  const exercise_founded = await Exercise.findAll({
    where: {
      ...(filters?.isDelete && { deletedAt: { [Op.not]: null } }),
    },
    ...((filters?.isDelete || filters?.includeDeleted) && { paranoid: true }),
    include,
    transaction,
  });
  return exercise_founded;
};

export const getByIds = async (payload: number[], options?: { transaction?: Transaction }, filters?: GetAllExerciseFilters): Promise<Exercise[]> => {
  if (!payload[0]) throw new Error("No id was provided as an argument on finding.");
  const exercise_founded = await Exercise.findAll({ where: { id: payload, ...(filters?.isDelete && { deletedAt: { [Op.not]: null } }) }, ...((filters?.isDelete || filters?.includeDeleted) && { paranoid: true }), ...options, include });
  return exercise_founded;
};

export const getByDbNames = async (payload: string[], options?: { transaction?: Transaction }, filters?: GetAllExerciseFilters): Promise<Exercise[]> => {
  if (!payload[0]) throw new Error("No db_name was provided as an argument on finding.");
  const exercise_founded = await Exercise.findAll({ where: { db_name: payload, ...(filters?.isDelete && { deletedAt: { [Op.not]: null } }) }, ...((filters?.isDelete || filters?.includeDeleted) && { paranoid: true }), ...options, include });
  return exercise_founded;
};

export const getWhereEquipmentIds = async (payload: number[], options?: { transaction?: Transaction }, filters?: GetAllExerciseFilters): Promise<Exercise[]> => {
  if (!payload[0]) throw new Error("No db_name was provided as an argument on finding.");
  const exercise_founded = await Exercise.findAll({ where: { "$equipments.id$": payload, ...(filters?.isDelete && { deletedAt: { [Op.not]: null } }) }, ...((filters?.isDelete || filters?.includeDeleted) && { paranoid: true }), ...options, include });
  return exercise_founded;
};

export const getWhereMuscleGroupIds = async (payload: number[], options?: { transaction?: Transaction }, filters?: GetAllExerciseFilters): Promise<Exercise[]> => {
  if (!payload[0]) throw new Error("No db_name was provided as an argument on finding.");
  const exercise_founded = await Exercise.findAll({ where: { "$muscle_groups.id$": payload, ...(filters?.isDelete && { deletedAt: { [Op.not]: null } }) }, ...((filters?.isDelete || filters?.includeDeleted) && { paranoid: true }), ...options, include });
  return exercise_founded;
};
