import { Transaction } from "sequelize";
import * as Exercise_db from "./exercise.db";
import Exercise, { ExerciseInput, ExerciseOutput } from "../../models/Exercise.model";
import { GetAllExerciseFilters } from "./exercise.types";

export const validateExerciseExistsFactory = (db_name: string, options?: { transaction?: Transaction }, filters?: GetAllExerciseFilters) => {
  if (!db_name) throw new Error("Invalid number of args, please pass a db_name.");
  return Exercise_db.getByDbNames([db_name], options, filters);
};

export const createFactory = (payload: ExerciseInput[], options?: { transaction?: Transaction }): Promise<{ data: Exercise[] | null; transaction: Transaction }> => {
  return Exercise_db.create(payload, options);
};

export const updateFactory = (payload: ExerciseInput[], options?: { transaction?: Transaction }): Promise<{ data: Exercise[] | null; transaction: Transaction }> => {
  return Exercise_db.update(payload, options);
};

export const deleteFactory = (payload: ExerciseInput[], options?: { force?: boolean; transaction?: Transaction }): Promise<{ data: number | null; transaction: Transaction }> => {
  return Exercise_db.deleteByObject(payload, options);
};

export const deleteByIdsFactory = (payload: number[], options?: { force?: boolean; transaction?: Transaction }): Promise<{ data: number | null; transaction: Transaction }> => {
  return Exercise_db.deleteByIds(payload, options);
};

export const getAllFactory = (transaction?: Transaction, filters?: GetAllExerciseFilters): Promise<Exercise[]> => {
  return Exercise_db.getAll(transaction, filters);
};

export const getByIdsFactory = (payload: number[], options?: { transaction?: Transaction }, filters?: GetAllExerciseFilters): Promise<Exercise[]> => {
  return Exercise_db.getByIds(payload, options, filters);
};

export const getByDbNamesFactory = (payload: string[], options?: { transaction?: Transaction }, filters?: GetAllExerciseFilters): Promise<Exercise[]> => {
  return Exercise_db.getByDbNames(payload, options, filters);
};

export const getWhereEquipmentIdsFactory = (payload: number[], options?: { transaction?: Transaction }, filters?: GetAllExerciseFilters): Promise<Exercise[]> => {
  return Exercise_db.getWhereEquipmentIds(payload, options, filters);
};

export const getWhereMuscleGroupIdsFactory = (payload: number[], options?: { transaction?: Transaction }, filters?: GetAllExerciseFilters): Promise<Exercise[]> => {
  return Exercise_db.getWhereMuscleGroupIds(payload, options, filters);
};
