import { Optional } from "sequelize";

export type CreateExerciseDTO = {
  db_name: string;
};

export type UpdateExerciseDTO = {
  id: number;
  db_name: string;
};

export type FilterExerciseDTO = {
  isDeleted?: boolean;
  includeDeleted?: boolean;
};
