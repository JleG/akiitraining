import { Optional } from "sequelize";

export type CreateEquipmentDTO = {
  db_name: string;
};

export type UpdateEquipmentDTO = {
  id: number;
  db_name: string;
};

export type FilterEquipmentDTO = {
  isDeleted?: boolean;
  includeDeleted?: boolean;
};
