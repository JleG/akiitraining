export interface GetAllEquipmentFilters {
  isDelete?: boolean;
  includeDeleted?: boolean;
}
