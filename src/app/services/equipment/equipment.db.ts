import { Op, Transaction } from "sequelize";
import { sequelize } from "../../models";
import Equipment, { EquipmentInput, EquipmentOutput } from "../../models/Equipment.model";
import { GetAllEquipmentFilters } from "./equipment.types";

// Externals models
import Exercise from "../../models/Exercise.model";

const include = [
  {
    model: Exercise,
    as: "exercises",
    attributes: ["id", "db_name"],
  },
];

export const create = async (payload: EquipmentInput[], options?: { transaction?: Transaction }): Promise<{ data: Equipment[] | null; transaction: Transaction }> => {
  const db_name_list: string[] = [];
  for (const ex of payload) {
    const { db_name } = ex;
    if (!db_name || db_name_list.includes(db_name)) throw new Error("Invalid argument: db_name");
    db_name_list.push(db_name);
  }

  const transaction = options?.transaction ? options.transaction : await sequelize.transaction();

  const equipment_founded = await Equipment.findAll({ where: { db_name: db_name_list }, transaction });
  if (equipment_founded[0]) throw new Error(`Invalid argument: db_name "${equipment_founded[0].db_name}" already exists.`);

  try {
    const equipment_inserted = await Equipment.bulkCreate(payload, { transaction });
    return { data: equipment_inserted, transaction };
  } catch (error) {
    await transaction.rollback();
    return { data: null, transaction };
  }
};

export const upsert = async (payload: EquipmentInput[], options?: { transaction?: Transaction }): Promise<{ data: Equipment[] | null; transaction: Transaction }> => {
  const db_name_list: string[] = [];
  for (const ex of payload) {
    const { db_name } = ex;
    if (!db_name || db_name_list.includes(db_name)) throw new Error("Invalid argument: db_name");
    db_name_list.push(db_name);
  }

  const transaction = options?.transaction ? options.transaction : await sequelize.transaction();

  const equipment_founded = await Equipment.findAll({ where: { db_name: db_name_list }, transaction });
  if (equipment_founded[0]) throw new Error(`Invalid argument: db_name "${equipment_founded[0].db_name}" already exists.`);

  try {
    const equipment_upserted = await Equipment.bulkCreate(payload, {
      fields: ["db_name"],
      updateOnDuplicate: ["db_name"],
      transaction,
    });
    return { data: equipment_upserted, transaction };
  } catch (error) {
    await transaction.rollback();
    return { data: null, transaction };
  }
};

export const update = async (payload: EquipmentInput[], options?: { transaction?: Transaction }): Promise<{ data: Equipment[] | null; transaction: Transaction }> => {
  const db_name_list: string[] = [];
  for (const ex of payload) {
    const { db_name } = ex;
    if (!db_name) throw new Error("Invalid argument: db_name");
    db_name_list.push(db_name);
  }

  const transaction = options?.transaction ? options.transaction : await sequelize.transaction();

  // const equipment_founded = await Equipment.findAll({ where: { db_name: db_name_list }, transaction });
  // if (equipment_founded[0]) throw new Error(`Invalid argument: db_name "${equipment_founded[0].db_name}" already exists.`);

  try {
    const equipment_updated = await Equipment.bulkCreate(payload, {
      updateOnDuplicate: ["db_name"],
      transaction,
    });
    return { data: equipment_updated, transaction };
  } catch (error) {
    await transaction.rollback();
    return { data: null, transaction };
  }
};

export const deleteByObject = async (payload: EquipmentInput[], options?: { force?: boolean; transaction?: Transaction }): Promise<{ data: number | null; transaction: Transaction }> => {
  if (!payload[0]) throw new Error("No Equipment object was provided as an argument on deletion.");
  const id_list: number[] = [];
  for (const ex of payload) {
    const { id } = ex;
    if (!id) throw new Error("Invalid argument: id");
    id_list.push(id);
  }

  const transaction = options?.transaction ? options.transaction : await sequelize.transaction();

  try {
    const equipment_deleted = await Equipment.destroy({ where: { id: id_list }, ...options });
    if (equipment_deleted === 0) throw new Error(`Error when deleting one or more Equipment object(s).`);
    return { data: equipment_deleted, transaction };
  } catch (error) {
    await transaction.rollback();
    return { data: null, transaction };
  }
};

export const deleteByIds = async (payload: number[], options?: { force?: boolean; transaction?: Transaction }): Promise<{ data: number | null; transaction: Transaction }> => {
  if (!payload[0]) throw new Error("No ids was provided as an argument on deletion.");

  const transaction = options?.transaction ? options.transaction : await sequelize.transaction();

  try {
    const equipment_deleted = await Equipment.destroy({ where: { id: payload }, ...options });
    if (equipment_deleted === 0) throw new Error(`Error when deleting one or more Equipment object(s) with id list.`);
    return { data: equipment_deleted, transaction };
  } catch (error) {
    await transaction.rollback();
    return { data: null, transaction };
  }
};

export const getAll = async (transaction?: Transaction, filters?: GetAllEquipmentFilters): Promise<Equipment[]> => {
  const equipment_founded = await Equipment.findAll({
    where: {
      ...(filters?.isDelete && { deletedAt: { [Op.not]: null } }),
    },
    ...((filters?.isDelete || filters?.includeDeleted) && { paranoid: true }),
    include,
    transaction,
  });
  return equipment_founded;
};

export const getByIds = async (payload: number[], options?: { transaction?: Transaction }, filters?: GetAllEquipmentFilters): Promise<Equipment[]> => {
  if (!payload[0]) throw new Error("No id was provided as an argument on finding.");
  const equipment_founded = await Equipment.findAll({ where: { id: payload, ...(filters?.isDelete && { deletedAt: { [Op.not]: null } }) }, ...((filters?.isDelete || filters?.includeDeleted) && { paranoid: true }), ...options, include });
  return equipment_founded;
};

export const getByDbNames = async (payload: string[], options?: { transaction?: Transaction }, filters?: GetAllEquipmentFilters): Promise<Equipment[]> => {
  if (!payload[0]) throw new Error("No db_name was provided as an argument on finding.");
  const equipment_founded = await Equipment.findAll({ where: { db_name: payload, ...(filters?.isDelete && { deletedAt: { [Op.not]: null } }) }, ...((filters?.isDelete || filters?.includeDeleted) && { paranoid: true }), ...options, include });
  return equipment_founded;
};
