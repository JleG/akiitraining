import { Transaction } from "sequelize";
import * as Equipment_db from "./equipment.db";
import Equipment, { EquipmentInput, EquipmentOutput } from "../../models/Equipment.model";
import { GetAllEquipmentFilters } from "./equipment.types";

export const validateEquipmentExistsFactory = (db_name: string, options?: { transaction?: Transaction }, filters?: GetAllEquipmentFilters) => {
  if (!db_name) throw new Error("Invalid number of args, please pass a db_name.");
  return Equipment_db.getByDbNames([db_name], options, filters);
};

export const createFactory = (payload: EquipmentInput[], options?: { transaction?: Transaction }): Promise<{ data: Equipment[] | null; transaction: Transaction }> => {
  return Equipment_db.create(payload, options);
};

export const updateFactory = (payload: EquipmentInput[], options?: { transaction?: Transaction }): Promise<{ data: Equipment[] | null; transaction: Transaction }> => {
  return Equipment_db.update(payload, options);
};

export const deleteFactory = (payload: EquipmentInput[], options?: { force?: boolean; transaction?: Transaction }): Promise<{ data: number | null; transaction: Transaction }> => {
  return Equipment_db.deleteByObject(payload, options);
};

export const deleteByIdsFactory = (payload: number[], options?: { force?: boolean; transaction?: Transaction }): Promise<{ data: number | null; transaction: Transaction }> => {
  return Equipment_db.deleteByIds(payload, options);
};

export const getAllFactory = (transaction?: Transaction, filters?: GetAllEquipmentFilters): Promise<Equipment[]> => {
  return Equipment_db.getAll(transaction, filters);
};

export const getByIdsFactory = (payload: number[], options?: { transaction?: Transaction }, filters?: GetAllEquipmentFilters): Promise<Equipment[]> => {
  return Equipment_db.getByIds(payload, options, filters);
};
export const getByDbNamesFactory = (payload: string[], options?: { transaction?: Transaction }, filters?: GetAllEquipmentFilters): Promise<Equipment[]> => {
  return Equipment_db.getByDbNames(payload, options, filters);
};
