import express, { Application } from "express";
import sequelize_fixtures from "sequelize-fixtures";

require("./app/config/custom.validate");

const app: Application = express();

// Body pardins Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Init routes
require(`${__dirname}/app/routes`)(app);

app.get("*", (req, res) => {
  res.status(404).json({ status: 404, error: "Page not found." });
});

require(`${__dirname}/app/fixtures/data.js`);

// Init the database connection and Sequelize syncronization.
const { sequelize } = require(`${__dirname}/app/models`);

const db_force: Boolean = process.env.DB_FORCE === "true" ? true : false;
const db_alter: Boolean = process.env.DB_ALTER === "true" ? true : false;

try {
  sequelize
    .sync({ force: db_force, alter: db_alter })
    .then(async ({ models }: any) => {
      if (db_force || db_alter) {
        await sequelize_fixtures.loadFiles([`${__dirname}/app/fixtures/equipment_data.json`, `${__dirname}/app/fixtures/muscleGroup_data.json`, `${__dirname}/app/fixtures/exercise_data.json`], models);
      }
      // Server initialization
      const PORT = process.env.PORT || 5000;
      app.listen(PORT, () => {
        // logger.info("Server listening on port: ${PORT}.");
        console.log({
          starting: `||====> Server listening on port: ${PORT}. <====||`,
        });
      });
    })
    .catch(() => {
      console.error("||=//=> Something went wront with the drop and re-sync db. <=//=||");
    });
} catch (error) {
  console.error(`Error occurred:`);
  console.error(error);
}

module.exports = app;

/*






const express = require("express");
const session = require("express-session");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const MySQLStore = require("express-mysql-session")(session);
// const passport = require("passport");
const sequelize_fixtures = require("sequelize-fixtures");
const config = require("./app/config/config");
const expressWinston = require("express-winston");
const { transports, format } = require("winston");

require("./app/config/custom.validate");

// Session Store options
const sessionStore = new MySQLStore({
  host: config.db.host,
  port: config.db.port,
  database: config.db.database,
  user: config.db.user,
  password: config.db.password,
});

const app = express();

// Cors setup
var corsOptions = {
  origin: (origin, callback) => {
    if ((process.env.NODE_ENV === "dev" && origin === undefined) || process.env.CORS_ORIGIN.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(new Error("Not allowed by CORS"));
    }
  },
  credentials: true,
};
app.use(cors(corsOptions));

// Express setup
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser(config.session_secret));

// Session setup
app.set("trust proxy", 1);
app.use(
  session({
    resave: true,
    rolling: true,
    saveUninitialized: false,
    secret: config.session_secret,
    store: sessionStore,
    cookie: {
      httpOnly: true,
      secure: false, // true if https
      maxAge: 1000 * 60 * 60 * 24 * 365,
    },
  })
);

// Passport authentication
// app.use(passport.initialize());
// app.use(passport.session());

// LOGGING
const { logger, requestLogger } = require("./app/config/logger");

expressWinston.requestWhitelist.push("body");
expressWinston.responseWhitelist.push("body");

app.use(
  expressWinston.logger({
    winstonInstance: logger,
    statusLevels: true,
    exitOnError: false,
  })
);

// Import all routes
// require("./app/middlewares/passport.mw");
require(`${__dirname}/app/routes`)(app);

app.get("*", (req, res) => {
  res.status(404).json({ status: 404, error: "Page not found." });
});

app.use(
  expressWinston.errorLogger({
    winstonInstance: requestLogger,
    statusLevels: true,
    exitOnError: false,
  })
);

// Init the database connection and Sequelize syncronization.
const models = require(`${__dirname}/app/models`);

const db_force = process.env.DB_FORCE === "true" ? true : false;
const db_alter = process.env.DB_ALTER === "true" ? true : false;

models.sequelize
  .sync({ force: db_force, alter: db_alter })
  .then(() => {
    // Loading data to database.
    if (db_force || db_alter) {
      sequelize_fixtures
        .loadFiles(
          [
            // `${__dirname}/app/fixtures/role_data.js`, // List of Roles
          ],
          models
        )
        .then(() => {
          console.log({ data_test: `||-|-> Fixtures loaded in the database.` });
        });
    }
    // Server initialization
    const PORT = process.env.PORT || 5000;
    app.listen(PORT, () => {
      // logger.info("Server listening on port: ${PORT}.");
      console.log({
        starting: `||====> Server listening on port: ${PORT}. <====||`,
      });
    });
  })
  .catch((err) => {
    console.log({
      err,
      error: "||=//=> Something went wront with the drop and re-sync db. <=//=||",
    });
  });

module.exports = app;
*/
